﻿namespace DigiNote.ViewModels
{
    using DigiNote.Interfaces;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Windows.Data;

    public class OrderVM : INPCBase
    {
        IFachkonzept fachkonzept;
        private string comment;
        private List<GroupItemVM> groupList;
        private ICollectionView items;
        private GroupItemVM selectedItem;
        private string amount;
        private bool isSaveButtonEnabled;

        public OrderVM(IFachkonzept fachkonzept)
        {
            this.fachkonzept = fachkonzept;
            IsSaveButtonEnabled = false;
            Comment = "";
            groupList = new List<GroupItemVM>();

            foreach (var group in fachkonzept.GetGroups())
            {
                var articles = fachkonzept.GetArticlesFromGroup(group.ID);
                if (articles.Count() != 0)
                {
                    foreach (var article in articles)
                    {
                        var groupVM = new GroupItemVM(article, group);
                        groupList.Add(groupVM);
                    }
                }
                else
                {
                    var groupVM = new GroupItemVM(null, group);
                    groupList.Add(groupVM);
                }
            }

            foreach (var article in fachkonzept.GetArticles())
            {
                if (fachkonzept.GetGroupsFromArticle(article.ID).Count() == 0)
                {
                    var groupVM = new GroupItemVM(article, null);
                    groupList.Add(groupVM);
                }
            }
            this.Items = new ListCollectionView(groupList);
            this.Items.GroupDescriptions.Add(new PropertyGroupDescription("GroupName"));
        }

        public GroupItemVM SelectedItem
        {
            get{return selectedItem;}
            set 
            {
                if (selectedItem != value) 
                {
                    selectedItem = value;
                    if (selectedItem != null)
                        IsSaveButtonEnabled = true;
                    else
                        IsSaveButtonEnabled = false;
                    NotifyPropertyChanged("SelectedItem");
                }
            }
        }

        public string Comment
        {
            get { return comment; }
            set
            {
                if (comment != value)
                {
                    comment = value;
                    NotifyPropertyChanged("Comment");
                }
            }
        }

        public string PopUpTitle { get { return "Neue Bestellung"; } }

        public ICollectionView Items
        {
            get { return items; }
            private set
            {
                if (items != value)
                {
                    items = value;
                    NotifyPropertyChanged("Items");
                }
            }
        }

        public string Amount 
        {
            get { return amount; }
            set
            {
                if (amount != value) 
                {
                    
                    amount = value;
                    NotifyPropertyChanged("Amount");
                }
            }
        }

        public bool IsSaveButtonEnabled
        {
            get { return isSaveButtonEnabled; }
            set
            {
                if (isSaveButtonEnabled != value)
                {
                    isSaveButtonEnabled = value;
                    NotifyPropertyChanged("IsSaveButtonEnabled");
                }
            }
        }
    }
}
