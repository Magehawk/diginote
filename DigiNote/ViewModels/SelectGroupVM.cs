﻿namespace DigiNote.ViewModels
{
    using System.Collections.Generic;
    using Interfaces.Business;
    public class SelectGroupVM:INPCBase
    {
        private IEnumerable<IGroup> groups;

        public SelectGroupVM(IEnumerable<IGroup> groups)
        {
            this.groups = groups;
        }

        public string PopUpTitle { get { return "Gruppenauswahl"; } }

        public string Header
        {
            get { return "Gruppe"; }
        }

        public IGroup SelectedItem { get; set; }

        public IEnumerable<IGroup> Items
        {
            get { return groups; }
            private set 
            {
                if (groups != value)
                {
                    groups = value;
                    NotifyPropertyChanged("Items");
                }
            }
        }
    }
}
