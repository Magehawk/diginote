﻿namespace DigiNote.ViewModels
{
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using DigiNote.Views;
    using System.Windows;
using System.Collections.Generic;
    using DigiNote.Interfaces.Business;
    using System.Text.RegularExpressions;
using DigiNote.Interfaces;

    public class ArticleVM : INPCBase
    {
        #region Fields
        private IFachkonzept fachkonzept;
        private string name;
        private bool isNameEmpty;
        private string price;
        private bool isPriceEmpty;
        private bool isSaveButtonEnabled;
        private ObservableCollection<IGroup> groups;
        #endregion //Fields

        #region Constructors
        public ArticleVM(IFachkonzept fachkonzept)
        {
            this.fachkonzept = fachkonzept;
            Name = "Name";
            IsNameEmpty = true;
            Price = "Preis";
            IsPriceEmpty = true;
            IsSaveButtonEnabled = false;
            AddGroupToListCommand = new BaseCommand(ExecuteAddGroupToListCommand);
            groups = new ObservableCollection<IGroup>();
        }
        #endregion //Constructors

        #region Properties
        public ObservableCollection<IGroup> Groups
        {
            get { return groups; }
        }

        public ICommand AddGroupToListCommand { get; private set; }

        public string PopUpTitle { get { return "Neuer Artikel"; } }

        public string Name
        {
            get { return name; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    IsNameEmpty = true;
                else
                    IsNameEmpty = false;
                if (name != value)
                {
                    name = value;
                    NotifyPropertyChanged("Name");
                }
            }
        }

        public bool IsNameEmpty
        {
            get { return isNameEmpty; }
            set
            {
                if (isNameEmpty != value)
                {
                    isNameEmpty = value;
                    CheckSaveButtonState();
                    NotifyPropertyChanged("IsNameEmpty");
                }
            }
        }

        public string Price
        {
            get { return price; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    IsPriceEmpty = true;
                else
                    IsPriceEmpty = false;
                if (price != value )
                {
                        price = value;
                    NotifyPropertyChanged("Price");
                }
            }
        }

        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9.-]+"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }

        public bool IsPriceEmpty
        {
            get { return isPriceEmpty; }
            set
            {
                if (isPriceEmpty != value)
                {
                    isPriceEmpty = value;
                    CheckSaveButtonState();
                    NotifyPropertyChanged("IsPriceEmpty");
                }
            }
        }

        public bool IsSaveButtonEnabled
        {
            get { return isSaveButtonEnabled; }
            set
            {
                if (isSaveButtonEnabled != value)
                {
                    isSaveButtonEnabled = value;
                    NotifyPropertyChanged("IsSaveButtonEnabled");
                }
            }
        }
        #endregion //Properties

        #region Methods
        private void CheckSaveButtonState()
        {
            if (IsNameEmpty)
                IsSaveButtonEnabled = false;
            else
                IsSaveButtonEnabled = true;
        }

        private void ExecuteAddGroupToListCommand(object obj)
        {
            var dialog = new PopUpWindow()
            {
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            };
            var vm = new SelectGroupVM(fachkonzept.GetGroups());

            dialog.DataContext = vm;
            if (dialog.ShowDialog() == true)
            {
                if (!Groups.Contains(vm.SelectedItem))
                    Groups.Add(vm.SelectedItem);
            }
        }
        #endregion //Methods
    }
}
