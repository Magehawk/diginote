﻿namespace DigiNote.ViewModels
{
    using DigiNote.Interfaces.Business;
    using System;
    using System.Windows.Media.Imaging;
    using System.Windows.Input;

    public class GroupItemVM : INPCBase
    {
        public readonly IArticle article;
        private IGroup group;
        private string groupName;
        private string name;
        private string price;


        public GroupItemVM(IArticle article, IGroup group)
        {
            if (article != null)
            {
                this.article = article;
                this.group = group;
                Name = article.Name;
                Price = article.Preis.ToString();
                IsEmpty = false;
            }
            else
            {
                this.group = group;
                Name = "";
                Price = "";
                IsEmpty = true;
            }
            if (group != null)
                GroupName = group.Name;
            else
                GroupName = "";


            DeleteArticleCommand = new BaseCommand(ExecuteDeleteArticleCommand);
        }

        public event EventHandler DeleteArticleEvent;
        public event EventHandler DeleteGroupEvent;

        public ICommand DeleteArticleCommand { get; private set; }

        public string Price
        {
            get { return price; }
            set
            {
                if (price != value)
                {
                    price = value;
                    NotifyPropertyChanged("Price");
                }
            }
        }

        public string GroupName
        {
            get { return groupName; }
            set
            {
                if (groupName != value)
                {
                    groupName = value;
                    NotifyPropertyChanged("GroupName");
                }
            }
        }

        public string Name
        {
            get { return name; }
            set
            {
                if (name != value)
                {
                    name = value;
                    NotifyPropertyChanged("Name");
                }
            }
        }

        private void ExecuteDeleteArticleCommand(object obj)
        {
            if (DeleteArticleEvent != null)
            {
                DeleteArticleEvent(article, EventArgs.Empty);
            }
        }

        public bool IsEmpty { get; set; }
    }
}
