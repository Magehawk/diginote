﻿namespace DigiNote.ViewModels
{
    using DigiNote.Interfaces;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Controls;
    using System;
    using System.Windows.Input;
    using Microsoft.Win32;

    public class ProfileVM : INPCBase
    {
        #region Fields
        IFachkonzept fachkonzept;
        private string userName;
        private bool isUserNameEmpty;
        private string firstName;
        private bool isFirstNameEmpty;
        private string lastName;
        private bool isLastNameEmpty;
        private string password;
        private bool isPasswordEmpty;
        private string repeatedPassword;
        private bool isRepeatedPasswordEmpty;
        private bool isSaveButtonEnabled;
        private BitmapSource image;
        #endregion //Fields

        #region Constructors
        public ProfileVM(IFachkonzept fachkonzept)
        {
            this.fachkonzept = fachkonzept;
            UserName = "Benutzername";
            IsUserNameEmpty = true;
            FirstName = "Vorname";
            IsFirstNameEmpty = true;
            LastName = "Nachname";
            IsLastNameEmpty = true;
            Password = string.Empty;
            RepeatedPassword = string.Empty;
            IsSaveButtonEnabled = false;
            //fix for empty profile picture
            var image = new BitmapImage();
            image.BeginInit();
            image.UriSource = new System.Uri(@"\Resources\img\Dummy.png", System.UriKind.Relative);
            image.EndInit();
            Image = image;
            SelectImageCommand = new BaseCommand(ExecuteSelectImageCommand);
        }
        #endregion //Constructors

        #region Propertys
        public ICommand SelectImageCommand { get; private set; }

        public string PopUpTitle { get { return "Neues Profil"; } }

        public string UserName
        {
            get { return userName; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    IsUserNameEmpty = true;
                else
                    IsUserNameEmpty = false;
                if (userName != value)
                {
                    userName = value;
                    CheckSaveButtonState();
                    NotifyPropertyChanged("UserName");
                }
            }
        }

        public bool IsUserNameEmpty
        {
            get { return isUserNameEmpty; }
            set
            {
                if (isUserNameEmpty != value)
                {
                    isUserNameEmpty = value;
                    NotifyPropertyChanged("IsUserNameEmpty");
                }
            }
        }

        public string FirstName
        {
            get { return firstName; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    IsFirstNameEmpty = true;
                else
                    IsFirstNameEmpty = false;
                if (firstName != value)
                {
                    firstName = value;
                    CheckSaveButtonState();
                    NotifyPropertyChanged("FirstName");
                }
            }
        }

        public bool IsFirstNameEmpty
        {
            get { return isFirstNameEmpty; }
            set
            {
                if (isFirstNameEmpty != value)
                {
                    isFirstNameEmpty = value;
                    NotifyPropertyChanged("IsFirstNameEmpty");
                }
            }
        }

        public string LastName
        {
            get { return lastName; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    IsLastNameEmpty = true;
                else
                    IsLastNameEmpty = false;
                if (lastName != value)
                {
                    lastName = value;
                    CheckSaveButtonState();
                    NotifyPropertyChanged("LastName");
                }
            }
        }

        public bool IsLastNameEmpty
        {
            get { return isLastNameEmpty; }
            set
            {
                if (isLastNameEmpty != value)
                {
                    isLastNameEmpty = value;
                    NotifyPropertyChanged("IsLastNameEmpty");
                }
            }
        }

        public string Password
        {
            get { return password; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    IsPasswordEmpty = true;
                else
                    IsPasswordEmpty = false;
                if (password != value)
                {
                    password = value;
                    CheckSaveButtonState();
                    NotifyPropertyChanged("Password");
                }
            }
        }

        public bool IsPasswordEmpty
        {
            get { return isPasswordEmpty; }
            set
            {
                if (isPasswordEmpty != value)
                {
                    isPasswordEmpty = value;
                    NotifyPropertyChanged("IsPasswordEmpty");
                }
            }
        }

        public string RepeatedPassword
        {
            get { return repeatedPassword; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    IsRepeatedPasswordEmpty = true;
                else
                    IsRepeatedPasswordEmpty = false;
                if (repeatedPassword != value)
                {
                    repeatedPassword = value;
                    CheckSaveButtonState();
                    NotifyPropertyChanged("RepeatedPassword");
                }
            }
        }

        public bool IsRepeatedPasswordEmpty
        {
            get { return isRepeatedPasswordEmpty; }
            set
            {
                if (isRepeatedPasswordEmpty != value)
                {
                    isRepeatedPasswordEmpty = value;
                    NotifyPropertyChanged("IsRepeatedPasswordEmpty");
                }
            }
        }

        public bool IsSaveButtonEnabled
        {
            get { return isSaveButtonEnabled; }
            set
            {
                if (isSaveButtonEnabled != value)
                {
                    isSaveButtonEnabled = value;
                    NotifyPropertyChanged("IsSaveButtonEnabled");
                }
            }
        }

        public BitmapSource Image
        {
            get { return image; }
            set
            {
                if (image != value)
                {
                    image = value;
                    NotifyPropertyChanged("Image");
                }
            }
        }
        #endregion //Propertys

        #region Methods
        private void CheckSaveButtonState()
        {
            if (IsUserNameEmpty || IsFirstNameEmpty || IsLastNameEmpty ||
                IsPasswordEmpty || IsRepeatedPasswordEmpty ||
                Password != RepeatedPassword)
                IsSaveButtonEnabled = false;
            else
                IsSaveButtonEnabled = true;
        }

        private void ExecuteSelectImageCommand(object obj)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Bilddateien (*.BMP;*.JPG;*.JPEG;*.PNG;*.GIF)|*.BMP;*.JPG;*.JPEG;*.PNG;*.GIF";
            if (dialog.ShowDialog() == true)
            {
                var image = new System.Windows.Media.Imaging.BitmapImage();
                image.BeginInit();
                image.UriSource = new System.Uri(dialog.FileName, System.UriKind.Absolute);
                image.EndInit();
                if (image.PixelHeight > 96 || image.PixelWidth > 96)
                    System.Windows.MessageBox.Show("Das Bild ist zu groß! (max 96x96)", "Fehler", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                else
                    Image = image;
            }
        }
        #endregion //Methods
    }
}
