﻿namespace DigiNote.ViewModels
{
    using DigiNote.Interfaces;
    using System.Windows.Input;
    using System.Windows;

    public class LoginVM : INPCBase
    {
        IFachkonzept fachkonzept;
        private string userName;
        private bool isUserNameEmpty;
        private string password;
        private bool isPasswordEmpty;
        private bool isLoginButtonEnabled;
        private Window window;

        public LoginVM(IFachkonzept fachkonzept, Window window)
        {
            this.fachkonzept = fachkonzept;
            this.window = window;
            userName = "Benutzername";
            IsUserNameEmpty = true;
            password = string.Empty;
            IsPasswordEmpty = true;
            IsLoginButtonEnabled = false;
            LoginCommand = new BaseCommand(ExecuteLoginCommand);
            fachkonzept.PerformLogIn();
        }

        public string WindowTitle { get { return "DigiNote - Anmeldung"; } }
        public ICommand LoginCommand { get; private set; }

        public string UserName
        {
            get { return userName; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    IsUserNameEmpty = true;
                else
                    IsUserNameEmpty = false;
                if (userName != value)
                {
                    userName = value;
                    NotifyPropertyChanged("UserName");
                }
            }
        }

        public bool IsUserNameEmpty
        {
            get { return isUserNameEmpty; }
            set
            {
                if (isUserNameEmpty != value)
                {
                    isUserNameEmpty = value;
                    CheckSaveButtonState();
                    NotifyPropertyChanged("IsUserNameEmpty");
                }
            }
        }

        public string Password 
        {
            get { return password; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    IsPasswordEmpty = true;
                else
                    IsPasswordEmpty = false;
                if (password != value)
                {
                    password = value;
                    NotifyPropertyChanged("Password");
                }
            }
        }

        public bool IsPasswordEmpty
        {
            get { return isPasswordEmpty; }
            set
            {
                if (isPasswordEmpty != value)
                {
                    isPasswordEmpty = value;
                    CheckSaveButtonState();
                    NotifyPropertyChanged("IsPasswordEmpty");
                }
            }
        }

        public bool IsLoginButtonEnabled
        {
            get { return isLoginButtonEnabled; }
            set
            {
                if (isLoginButtonEnabled != value)
                {
                    isLoginButtonEnabled = value;
                    NotifyPropertyChanged("IsLoginButtonEnabled");
                }
            }
        }

        private void ExecuteLoginCommand(object obj)
        {
            fachkonzept.ValidateUser(userName, password);
            if (fachkonzept.IsLoginSuccessfull)
            {
                window.Close();
            }
            else
                System.Windows.MessageBox.Show("Anmeldung fehlgeschlagen!", "Fehler", MessageBoxButton.OK, MessageBoxImage.Hand);
        }

        private void CheckSaveButtonState()
        {
            if (IsUserNameEmpty || IsPasswordEmpty)
                IsLoginButtonEnabled = false;
            else
                IsLoginButtonEnabled = true;
        }
    }
}
