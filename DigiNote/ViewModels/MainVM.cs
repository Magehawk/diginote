﻿namespace DigiNote.ViewModels
{
    using DigiNote.Interfaces;
    using System.Windows;
    using System.Windows.Input;
    using DigiNote.Views;
    using System.Windows.Controls;
    using System.Windows.Media.Imaging;
    using System.Collections.ObjectModel;
    using System.Linq;

    public class MainVM : INPCBase
    {
        IFachkonzept fachkonzept;
        private Window window;
        private readonly ObservableCollection<OrderItemVM> orders;
        private string comments;
        private double sum;
        public MainVM(IFachkonzept fachkonzept, Window window)
        {
            this.fachkonzept = fachkonzept;
            this.window = window;
            orders=new ObservableCollection<OrderItemVM>();
            OpenProfilManagerCommand = new BaseCommand(ExecuteOpenProfileManagerCommand);
            OpenItemManagerCommand = new BaseCommand(ExecuteOpenItemManagerCommand);
            AddItemToShoppingListCommand = new BaseCommand(ExecuteAddItemToShoppingListCommand);
            LogOutCommand = new BaseCommand(ExecuteLogOutCommand);
            LoadOrders();
        }

        public ICommand OpenProfilManagerCommand { get; private set; }
        public ICommand OpenItemManagerCommand { get; private set; }
        public ICommand AddItemToShoppingListCommand { get; private set; }
        public ICommand LogOutCommand { get; private set; }
        public string WindowTitle { get { return "DigiNote - Home"; } }

        public string UserName
        {
            get { return fachkonzept.CurentUser.UserName; }
            //set
            //{
            //    if (fachkonzept.CurentUser.UserName != value)
            //    {
            //        //fachkonzept.CurentUser.UserName = value;
            //        NotifyPropertyChanged("UserName");
            //    }
            //}
        }

        public BitmapSource Image
        {
            get { return fachkonzept.CurentUser.Image; }
            //set
            //{
            //    if (fachkonzept.CurentUser.Image != value)
            //    {
            //        fachkonzept.CurentUser.Image = value;
            //        NotifyPropertyChanged("Image");
            //    }
            //}
        }

        public string Comments 
        {
            get { return comments; }
            private set
            {
                if (comments != value)
                {
                    comments = value;
                    NotifyPropertyChanged("Comments");
                }
            }
        }

        public double Sum
        {
            get { return sum; }
            private set
            {
                if (sum != value)
                {
                    sum = value;
                    NotifyPropertyChanged("Sum");
                }
            }
        }

        public ObservableCollection<OrderItemVM> Orders
        {
            get{return orders;}
        }

        private void ExecuteOpenProfileManagerCommand(object param)
        {
            var profileManager = new ProfileManagerVM(fachkonzept, window, this);
            window.DataContext = profileManager;
        }

        private void ExecuteOpenItemManagerCommand(object param)
        {
            var itemManager = new ItemManagerVM(fachkonzept, window, this);
            window.DataContext = itemManager;
        }

        private void ExecuteAddItemToShoppingListCommand(object param)
        {
            var dialog = new PopUpWindow()
            {
                Owner = window,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            };
            var vm = new OrderVM(fachkonzept);
            dialog.DataContext = vm;
            if (dialog.ShowDialog() == true)
            {
                int amount = 1;
                
                if (int.TryParse(vm.Amount, out amount))
                {
                    fachkonzept.CreateOrder(vm.SelectedItem.article.ID, amount, vm.Comment);
                }
                else
                {
                    fachkonzept.CreateOrder(vm.SelectedItem.article.ID, 1, vm.Comment);
                }
                
                LoadOrders();
            }
        }

        private void LoadOrders()
        {
            Orders.Clear();
            Comments = "";
            var articles = fachkonzept.GetArticles();
            var profiles = fachkonzept.GetProfiles();
            foreach(var order in fachkonzept.GetOrders())
            {
                var orderVM = Orders.Where(x=>x.ID==order.ArticleId);
                if(orderVM.Count()!=0)
                {
                    orderVM.First().Amount += order.Amount;
                }
                else
                {
                    var newOrderVM = new OrderItemVM(Orders.Count()+1,articles.Where(x=>x.ID==order.ArticleId).First());
                    newOrderVM.Amount=order.Amount;
                    Orders.Add(newOrderVM);
                }

                if (!string.IsNullOrWhiteSpace(order.Comment))
                {
                    string user="";
                    if(order.ProfileId==-1)
                        user="root";
                    else
                        user=profiles.Where(x=>x.ID==order.ProfileId).First().UserName;
                    
                    Comments += string.Format("{0} - {1}: {2} \n   {3}\n", 
                        order.CommentTimeStamp.ToString(), user ,articles.Where(x=>x.ID==order.ArticleId).First().Name, order.Comment);
                }
            }
            Sum=0.0;
            foreach (var order in Orders)
            {
                Sum += order.Sum;
            }
        }

        private void ExecuteLogOutCommand(object param)
        {
            fachkonzept.PerformLogOut();
            window.Close();
        }
    }
}
