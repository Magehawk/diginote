﻿namespace DigiNote.ViewModels
{
    using DigiNote.Interfaces.Business;

    public class OrderItemVM : INPCBase
    {
        private int id;
        private string name;
        private int amount;
        private double price;
        private double sum;
        private int index;

        public OrderItemVM(int index,IArticle article)
        {
            Index = index;
            ID = article.ID;
            Name = article.Name;
            Price = article.Preis;
        }
        public int Index
        {
            get { return index; }
            set
            {
                if (index != value)
                {
                    index = value;
                    NotifyPropertyChanged("Index");
                }
            }
        }

        public int ID
        {
            get { return id; }
            set
            {
                if (id != value)
                {
                    id = value;
                    NotifyPropertyChanged("ID");
                }
            }
        }

        public string Name
        {
            get { return name; }
            set
            {
                if (name != value)
                {
                    name = value;
                    NotifyPropertyChanged("Name");
                }
            }
        }

        public int Amount
        {
            get { return amount; }
            set
            {
                if (amount != value)
                {
                    amount = value;
                    Sum = amount * price;
                    NotifyPropertyChanged("Amount");
                }
            }
        }

        public double Price
        {
            get { return price; }
            set
            {
                if (price != value)
                {
                    price = value;
                    NotifyPropertyChanged("Price");
                }
            }
        }

        public double Sum
        {
            get { return sum; }
            set
            {
                if (sum != value)
                {
                    sum = value;
                    NotifyPropertyChanged("Sum");
                }
            }
        }
    }
}
