﻿namespace DigiNote.ViewModels
{
    using System.Collections.Generic;
    using DigiNote.Interfaces.Business;

    public class SelectArticleVM : INPCBase
    {
        private string header;

        private IEnumerable<IArticle> articles;

        public SelectArticleVM(IEnumerable<IArticle> articles)
        {
            this.articles = articles;
            Header = "Artikel";
        }

        public string PopUpTitle { get { return "Artikelauswahl"; } }

        public string Header
        {
            get { return header; }
            set
            {
                if (header != value)
                {
                    header = value;
                    NotifyPropertyChanged("Header");
                }
            }
        }

        public IArticle SelectedItem { get; set; }

        public IEnumerable<IArticle> Items
        {
            get { return articles; }
            private set
            {
                if (articles != value)
                {
                    articles = value;
                    NotifyPropertyChanged("Items");
                }
            }
        }
    }
}
