﻿namespace DigiNote.ViewModels
{
    using System.Collections.ObjectModel;
    using System.Windows;
    using System.Windows.Input;

    using DigiNote.Interfaces;
    using DigiNote.Views;
    using DigiNote.Interfaces.Business;

    public class GroupVM : INPCBase
    {
        #region Fields
        private IFachkonzept fachkonzept;
        private string name;
        private bool isNameEmpty;
        private bool isSaveButtonEnabled;
        
        private Window owner;
        #endregion //Fields

        #region Constructors
        public GroupVM(Window owner, IFachkonzept fachkonzept)
        {
            this.fachkonzept = fachkonzept;
            Name = "Name";
            this.owner = owner;
            IsNameEmpty = true;
            IsSaveButtonEnabled = false;
            AddArticleToListCommand = new BaseCommand(ExecuteAddArticleToListCommand);
            Items = new ObservableCollection<IArticle>();
        }
        #endregion //Constructors

        #region Properties
        public ObservableCollection<IArticle> Items
        {
            get;
            private set;
        }

        public ICommand AddArticleToListCommand { get; private set; }

        public string PopUpTitle { get { return "Neue Gruppe"; } }

        public string Name
        {
            get { return name; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    IsNameEmpty = true;
                else
                    IsNameEmpty = false;
                if (name != value)
                {
                    name = value;
                    NotifyPropertyChanged("Name");
                }
            }
        }

        public bool IsNameEmpty
        {
            get { return isNameEmpty; }
            set
            {
                if (isNameEmpty != value)
                {
                    isNameEmpty = value;
                    NotifyPropertyChanged("IsNameEmpty");
                    CheckSaveButtonState();
                }
            }
        }

        public bool IsSaveButtonEnabled
        {
            get { return isSaveButtonEnabled; }
            set
            {
                if (isSaveButtonEnabled != value)
                {
                    isSaveButtonEnabled = value;
                    NotifyPropertyChanged("IsSaveButtonEnabled");
                }
            }
        }
        #endregion //Properties

        #region Methods
        private void CheckSaveButtonState()
        {
            if (IsNameEmpty)
                IsSaveButtonEnabled = false;
            else
                IsSaveButtonEnabled = true;
        }

        private void ExecuteAddArticleToListCommand(object obj)
        {
            var dialog = new PopUpWindow()
            {
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            };
            var vm = new SelectArticleVM(fachkonzept.GetArticles());

            dialog.DataContext = vm;
            if (dialog.ShowDialog() == true)
            {
                if(!Items.Contains(vm.SelectedItem))
                    Items.Add(vm.SelectedItem);
            }
        }
        #endregion //Methods
    }
}
