﻿namespace DigiNote.ViewModels
{
    using DigiNote.Interfaces.Business;
    using System;
    using System.Windows.Media.Imaging;
    using System.Windows.Input;

    public class ProfileItemVM : INPCBase
    {
        private IProfile profile;
        private string userName;
        private string firstName;
        private string lastName;
        private BitmapSource image;

        public ProfileItemVM(IProfile profile)
        {
            this.profile = profile;
            UserName = profile.UserName;
            FirstName = profile.FirstName;
            LastName = profile.LastName;
            Image = profile.Image;
            DeleteProfileCommand = new BaseCommand(ExecuteDeleteProfileCommand);
        }

        public event EventHandler DeleteProfileEvent;

        public ICommand DeleteProfileCommand { get; private set; }

        public string UserName
        {
            get { return userName; }
            set
            {
                if (userName != value)
                {
                    userName = value;
                    NotifyPropertyChanged("UserName");
                }
            }
        }

        public string FirstName
        {
            get { return firstName; }
            set
            {
                if (firstName != value)
                {
                    firstName = value;
                    NotifyPropertyChanged("FirstName");
                }
            }
        }

        public string LastName
        {
            get { return lastName; }
            set
            {
                if (lastName != value)
                {
                    lastName = value;
                    NotifyPropertyChanged("LastName");
                }
            }
        }

        public BitmapSource Image
        {
            get { return image; }
            set
            {
                if (image != value)
                {
                    image = value;
                    NotifyPropertyChanged("Image");
                }
            }
        }

        private void ExecuteDeleteProfileCommand(object obj)
        {
            if (DeleteProfileEvent != null) 
            {
                DeleteProfileEvent(profile,EventArgs.Empty);
            }
        }
    }
}
