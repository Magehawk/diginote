﻿namespace DigiNote.ViewModels
{
    using DigiNote.Interfaces;
    using System.Windows;
    using System.Windows.Input;
    using DigiNote.Views;
    using System;
    using DigiNote.Interfaces.Business;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Data;

    public class ItemManagerVM : INPCBase
    {
        IFachkonzept fachkonzept;
        Window mainWindow;
        MainVM mainVM;
        private List<GroupItemVM> groupList;
        private ICollectionView items;

        public ItemManagerVM(IFachkonzept fachkonzept, Window mainWindow, MainVM mainVM)
        {
            this.fachkonzept = fachkonzept;
            this.mainWindow = mainWindow;
            this.mainVM = mainVM;
            this.groupList = new List<GroupItemVM>();

            NewGroupCommand = new BaseCommand(ExecuteNewGroupCommand);
            NewItemCommand = new BaseCommand(ExecuteNewItemCommand);
            BackCommand = new BaseCommand(ExecuteBackCommand);
            
            
            LoadItems();
        }
        public ICommand NewGroupCommand { get; set; }
        public ICommand NewItemCommand { get; set; }
        public ICommand BackCommand { get; set; }

        public string WindowTitle { get { return "Artikelverwaltung"; } }

        public ICollectionView Items
        {
            get { return items; }
            private set
            {
                if (items != value)
                {
                    items = value;
                    NotifyPropertyChanged("Items");
                }
            }
        }

        private void ExecuteNewGroupCommand(object obj)
        {
            var dialog = new PopUpWindow()
            {
                Owner = mainWindow,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            };
            var vm = new GroupVM(dialog, fachkonzept);
            dialog.DataContext = vm;
            if (dialog.ShowDialog() == true)
            {
                IGroup newGroup;
                newGroup = fachkonzept.CreateGroup(vm.Name);
                foreach (var article in vm.Items)
                    fachkonzept.AddArticleToGroup(newGroup.ID, article.ID);
                LoadItems();
            }
        }

        private void ExecuteNewItemCommand(object obj)
        {
            var dialog = new PopUpWindow()
            {
                Owner = mainWindow,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            };
            var vm = new ArticleVM(fachkonzept);
            dialog.DataContext = vm;
            if (dialog.ShowDialog() == true)
            {
                IArticle newArticle;
                double preis = 0;
                if (double.TryParse(vm.Price, out preis))
                    newArticle = fachkonzept.CreateArticle(vm.Name, Math.Round(preis, 2));
                else
                    newArticle = fachkonzept.CreateArticle(vm.Name, 0);
                foreach (var group in vm.Groups)
                    fachkonzept.AddArticleToGroup(group.ID, newArticle.ID);
                LoadItems();
            }

        }

        private void ExecuteBackCommand(object obj)
        {
            this.mainWindow.DataContext = mainVM;
        }

        private void LoadItems()
        {
            this.groupList.Clear();
            //var articles = fachkonzept.GetArticles();
            //get all grouped articles
            foreach (var group in fachkonzept.GetGroups())
            {
                var articles = fachkonzept.GetArticlesFromGroup(group.ID);
                if (articles.Count() != 0)
                {
                    foreach (var article in articles)
                    {
                        var groupVM = new GroupItemVM(article, group);
                        groupVM.DeleteArticleEvent += DeleteArticle;
                        groupVM.DeleteGroupEvent += DeleteGroup;
                        this.groupList.Add(groupVM);
                    }
                }
                else 
                {
                    var groupVM = new GroupItemVM(null, group);
                    groupVM.DeleteArticleEvent += DeleteArticle;
                    groupVM.DeleteGroupEvent += DeleteGroup;
                    this.groupList.Add(groupVM);
                }
            }

            foreach (var article in fachkonzept.GetArticles()) 
            {
                if(fachkonzept.GetGroupsFromArticle(article.ID).Count()==0)
                {
                    var groupVM = new GroupItemVM(article, null);
                    groupVM.DeleteArticleEvent += DeleteArticle;
                    this.groupList.Add(groupVM);
                }
            }
            this.Items = new ListCollectionView(groupList);
            this.Items.GroupDescriptions.Add(new PropertyGroupDescription("GroupName"));
        }

        private void DeleteArticle(object sender, EventArgs e)
        {
            fachkonzept.DeleteArticle(((IArticle)sender).ID);
            LoadItems();
        }
        private void DeleteGroup(object sender, EventArgs e)
        {
            fachkonzept.DeleteGroup(((IGroup)sender).ID);
            LoadItems();
        }
    }
}
