﻿namespace DigiNote.ViewModels
{
    using DigiNote.Interfaces;
    using System.Windows;
    using System.Windows.Input;
    using DigiNote.Views;
    using System.Collections.ObjectModel;
    using DigiNote.Interfaces.Business;
    using System;

    public class ProfileManagerVM : INPCBase
    {
        private IFachkonzept fachkonzept;
        private Window mainWindow;
        private MainVM mainVM;
        private readonly ObservableCollection<ProfileItemVM> profiles;

        public ProfileManagerVM(IFachkonzept fachkonzept, Window mainWindow, MainVM mainVM)
        {
            this.fachkonzept = fachkonzept;
            this.mainWindow = mainWindow;
            this.mainVM = mainVM;
            NewProfileCommand = new BaseCommand(ExecuteNewProfileCommand);
            BackCommand = new BaseCommand(ExecuteBackCommand);
            profiles = new ObservableCollection<ProfileItemVM>();
            LoadProfiles();
        }

        public ICommand NewProfileCommand { get; set; }
        public ICommand BackCommand { get; set; }
        public string WindowTitle { get { return "Profilverwaltung"; } }

        public ObservableCollection<ProfileItemVM> Profiles
        {
            get { return profiles; }
        }

        private void ExecuteNewProfileCommand(object obj)
        {
            var dialog = new PopUpWindow()
            {
                Owner = mainWindow,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            };
            var vm = new ProfileVM(fachkonzept);
            dialog.DataContext = vm;
            if (dialog.ShowDialog() == true)
            {
                var newProfile = fachkonzept.CreateProfile();
                fachkonzept.EditProfile(newProfile.ID, vm.UserName, vm.FirstName, vm.LastName, vm.Password, vm.Image);
                LoadProfiles();
            }
        }

        private void ExecuteBackCommand(object obj)
        {
            this.mainWindow.DataContext = mainVM;
        }

        private void LoadProfiles()
        {
            Profiles.Clear();
            foreach (IProfile profile in fachkonzept.GetProfiles())
            {
                var profileVM = new ProfileItemVM(profile);
                profileVM.DeleteProfileEvent+=DeleteProfile;
                Profiles.Add(profileVM);
            }
        }

        private void DeleteProfile(object sender, EventArgs e)
        {
            fachkonzept.DeleteProfile(((IProfile)sender).ID);
            LoadProfiles();
        }
    }
}
