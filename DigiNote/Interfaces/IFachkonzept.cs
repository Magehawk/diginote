﻿namespace DigiNote.Interfaces
{
    using System.Collections.Generic;
    using DigiNote.Interfaces.Business;
    using System.Windows.Media.Imaging;

    public enum NewProfileValidationStatus { Ok, AllreadyExisting, PasswordFailed}
    public enum NewItemValidationStatus { Ok, AllreadyExisting, IsEmpty }

    public interface IFachkonzept
    {
        #region SessionManagement
        IProfile CurentUser { get; }
        bool IsUserChanged { get; }
        bool IsLoginSuccessfull { get; }
        void ValidateUser(string userName, string password);
        void PerformLogIn();
        void PerformLogOut();
        #endregion //SessionManagement

        #region ProfileManagement
        IEnumerable<IProfile> GetProfiles();
        NewProfileValidationStatus ValidateNewPofile(string userName, string password, string confirmPassword);
        IProfile CreateProfile();
        void EditProfile(int id, string userName, string firstName, string lastName, string password, BitmapSource image);
        bool CanDeleteProfile(int id);
        void DeleteProfile(int id);
        #endregion //ProfileManagement

        #region GroupManagement
        IEnumerable<IGroup> GetGroups();
        NewItemValidationStatus ValidateNewGroup(string name);
        IGroup CreateGroup(string name);
        void EditGroup(int id, string name);
        bool CanDeleteGroup(int id);
        void DeleteGroup(int id);
        void AddArticleToGroup(int groupId, int articleId);
        void RemouveArticleFromGroup(int groupId, int articleId);
        IEnumerable<IArticle> GetArticlesFromGroup(int groupId);
        #endregion //GroupManagement

        #region ArticleManagement
        IEnumerable<IArticle> GetArticles();
        NewItemValidationStatus ValidateNewArticle(string name);
        IArticle CreateArticle(string name, double price);
        void EditArticle(int id, string name, double price);
        bool CanDeleteArticle(int id);
        void DeleteArticle(int id);
        IEnumerable<IGroup> GetGroupsFromArticle(int articleId);
        #endregion //ArticleManagement

        #region OrderManagement
        IEnumerable<IOrder> GetOrders();
        IOrder CreateOrder(int articleId, int ammount, string comment);
        void PurchaseItem(int articleId);
        #endregion //OrderManagement
    }
}