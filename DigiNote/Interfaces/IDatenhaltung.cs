﻿namespace DigiNote.Interfaces
{
    using DigiNote.Interfaces.Business;
    using System.Collections.Generic;

    public interface IDatenhaltung
    {
        #region Profile Persistance
        List<IProfile> ReadAllUsers();
        void SaveProfile(IProfile profile);
        void RemoveProfile(int id);
        #endregion //Profile Persistance

        #region Group Persistance
        List<IGroup> ReadAllGroups();
        void SaveGroup(IGroup group);
        void RemoveGroup(int id);
        #endregion //Profile Persistance

        #region Article Persistance
        List<IArticle> ReadAllArticles();
        void SaveArticle(IArticle article);
        void RemoveArticle(int id);
        #endregion //Profile Persistance

        #region Order Persistance
        List<IOrder> ReadAllOrders();
        void SaveOrder(IOrder order);
        void RemoveOrder(IOrder order);
        #endregion //Profile Persistance
    }
}
