﻿namespace DigiNote.Interfaces.Business
{
    public interface IArticle : IHaveId
    {
        string Name { get; set; }
        double Preis { get; }
    }
}
