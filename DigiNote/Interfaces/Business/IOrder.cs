﻿namespace DigiNote.Interfaces.Business
{
    using System;

    public interface IOrder
    {
        int ProfileId { get; }
        int ArticleId { get; }
        int Amount { get; }
        string Comment { get; set; }
        DateTime CommentTimeStamp { get; set; }
    }
}
