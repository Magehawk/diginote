﻿namespace DigiNote.Interfaces.Business
{
    public interface IHaveId
    {
        int ID { get; set; }
    }
}
