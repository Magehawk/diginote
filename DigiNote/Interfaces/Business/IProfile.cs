﻿namespace DigiNote.Interfaces.Business
{
    using System.Windows.Media.Imaging;

    public interface IProfile : IHaveId
    {
        string UserName { get; }
        string FirstName{ get; }
        string LastName{ get;  }
        string Password{ get; }
        BitmapSource Image { get; }
    }
}
