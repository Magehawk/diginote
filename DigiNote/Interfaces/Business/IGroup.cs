﻿namespace DigiNote.Interfaces.Business
{
    using System.Collections.Generic;

    public interface IGroup : IHaveId
    {
        string Name { get; set; }
        List<int> Articles {get;}
    }
}
