﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

namespace DigiNote.Views.Controls
{
    /// <summary>
    /// Interaktionslogik für Article.xaml
    /// </summary>
    public partial class Article : UserControl
    {
        public Article()
        {
            InitializeComponent();
        }
        private void TextBox_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var tb = (TextBox)sender;
            var vm = (DigiNote.ViewModels.ArticleVM)tb.DataContext;
            if (tb.Equals(tbx_Name) && vm.IsNameEmpty)
                tbx_Name.Text = string.Empty;
            else if (tb.Equals(tbx_Price) && vm.IsPriceEmpty)
                tbx_Price.Text = string.Empty;
        }

        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            var tb = (TextBox)sender;
            var vm = (DigiNote.ViewModels.ArticleVM)tb.DataContext;
            if (tb.Equals(tbx_Name) && vm.IsNameEmpty)
            {
                tbx_Name.Text = "Name";
                vm.IsNameEmpty = true;
            }
            else if (tb.Equals(tbx_Price) && vm.IsPriceEmpty)
            {
                tbx_Price.Text = "Preis";
                vm.IsPriceEmpty = true;
            }
        }
    }
}
