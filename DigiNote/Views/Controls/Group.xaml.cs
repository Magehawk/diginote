﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DigiNote.Views.Controls
{
    /// <summary>
    /// Interaktionslogik für Group.xaml
    /// </summary>
    public partial class Group : UserControl
    {
        public Group()
        {
            InitializeComponent();
        }

        private void TextBox_MouseDown(object sender, MouseButtonEventArgs e)
        {
                var tb = (TextBox)sender;
                var vm = (DigiNote.ViewModels.GroupVM)tb.DataContext;
                if (tb.Equals(tbx_Name) && vm.IsNameEmpty)
                    tbx_Name.Text = string.Empty;
        }

        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            var tb = (TextBox)sender;
            var vm = (DigiNote.ViewModels.GroupVM)tb.DataContext;
            if (tb.Equals(tbx_Name) && vm.IsNameEmpty)
            {
                tbx_Name.Text = "Name";
                vm.IsNameEmpty = true;
            }
        }
    }
}
