﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DigiNote.Views.Controls
{
    /// <summary>
    /// Interaktionslogik für Login.xaml
    /// </summary>
    public partial class Login : UserControl
    {
        public Login()
        {
            InitializeComponent();
        }

        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (sender is TextBox)
            {
                var tb = (TextBox)sender;
                var vm = (DigiNote.ViewModels.LoginVM)tb.DataContext;
                if (tb.Equals(tbx_UserName) && vm.IsUserNameEmpty)
                    tbx_UserName.Text = string.Empty;
            }
            else if (sender is PasswordBox)
            {
                var tb = (PasswordBox)sender;
                var vm = (DigiNote.ViewModels.LoginVM)tb.DataContext;
                if (vm.IsPasswordEmpty)
                    tbx_Password.Text = string.Empty;
            }
        }

        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            
            if (sender is TextBox)
            {
                var tb = (TextBox)sender;
                var vm = (DigiNote.ViewModels.LoginVM)tb.DataContext;
                if (tb.Equals(tbx_UserName) && vm.IsUserNameEmpty)
                {
                    tbx_UserName.Text = "Benutzername";
                    vm.IsUserNameEmpty = true;
                }
            }
            else if (sender is PasswordBox)
            {
                var tb = (PasswordBox)sender;
                var vm = (DigiNote.ViewModels.LoginVM)tb.DataContext;
                if( vm.IsPasswordEmpty)
                {
                    tbx_Password.Text = "Passwort";
                    vm.IsPasswordEmpty = true;
                }
            }
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            var vm = this.DataContext as DigiNote.ViewModels.LoginVM;
            vm.Password = passwordBox.Password;
        }
    }
}
