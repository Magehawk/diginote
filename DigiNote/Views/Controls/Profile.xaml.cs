﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DigiNote.Views.Controls
{
    /// <summary>
    /// Interaktionslogik für Profile.xaml
    /// </summary>
    public partial class Profile : UserControl
    {
        public Profile()
        {
            InitializeComponent();
        }

        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (sender is TextBox)
            {
                var tb = (TextBox)sender;
                var vm = (DigiNote.ViewModels.ProfileVM)tb.DataContext;
                if (tb.Equals(tbx_UserName) && vm.IsUserNameEmpty)
                    tbx_UserName.Text = string.Empty;
                else if (tb.Equals(tbx_FirstName) && vm.IsFirstNameEmpty)
                    tbx_FirstName.Text = string.Empty;
                else if (tb.Equals(tbx_LastName) && vm.IsLastNameEmpty)
                    tbx_LastName.Text = string.Empty;
            }
            else if (sender is PasswordBox)
            {
                var tb = (PasswordBox)sender;
                var vm = (DigiNote.ViewModels.ProfileVM)tb.DataContext;
                if (sender.Equals(passwordBox) && vm.IsPasswordEmpty)
                    tbx_Password.Text = string.Empty;
                else if (sender.Equals(repeatedPasswordBox) && vm.IsRepeatedPasswordEmpty)
                    tbx_RepeatedPassword.Text = string.Empty;

            }
        }

        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (sender is TextBox)
            {
                var tb = (TextBox)sender;
                var vm = (DigiNote.ViewModels.ProfileVM)tb.DataContext;
                if (tb.Equals(tbx_UserName) && vm.IsUserNameEmpty)
                {
                    tbx_UserName.Text = "Benutzername";
                    vm.IsUserNameEmpty = true;
                }
                else if (tb.Equals(tbx_FirstName) && vm.IsFirstNameEmpty)
                {
                    tbx_FirstName.Text = "Vorname";
                    vm.IsFirstNameEmpty = true;
                }
                else if (tb.Equals(tbx_LastName) && vm.IsLastNameEmpty)
                {
                    tbx_LastName.Text = "Nachname";
                    vm.IsLastNameEmpty = true;
                }
            }
            else if (sender is PasswordBox)
            {
                var tb = (PasswordBox)sender;
                var vm = (DigiNote.ViewModels.ProfileVM)tb.DataContext;
                if (sender.Equals(passwordBox) && vm.IsPasswordEmpty)
                    {
                    tbx_Password.Text = "Passwort";
                }
                else if (sender.Equals(repeatedPasswordBox) && vm.IsRepeatedPasswordEmpty)
                {
                    tbx_RepeatedPassword.Text = "Wiederholen";
                }

            }
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            var vm = this.DataContext as DigiNote.ViewModels.ProfileVM;
            vm.Password = passwordBox.Password;
        }

        private void RepeatedPassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            var vm = this.DataContext as DigiNote.ViewModels.ProfileVM;
            vm.RepeatedPassword = repeatedPasswordBox.Password;
        }


    }
}
