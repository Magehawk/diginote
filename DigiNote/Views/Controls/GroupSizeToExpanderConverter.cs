﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using DigiNote.ViewModels;

namespace DigiNote.Views.Controls
{
    public class GroupSizeToExpanderConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            CollectionViewGroup grp = (CollectionViewGroup)value;
            if (grp.ItemCount == 1)
            {
                if ((grp.Items[0] as GroupItemVM).IsEmpty)
                    return false;
            }
            
            return true; // ALTERNATIVLY grp.ItemCount;             
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
