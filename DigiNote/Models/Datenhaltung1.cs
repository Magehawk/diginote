﻿namespace DigiNote.Models
{
    using DigiNote.Interfaces;
    using DigiNote.Interfaces.Business;
    using DigiNote.Models.NSDatenhaltung1;
    using System.Collections.Generic;

    public class Datenhaltung1 : IDatenhaltung
    {
        public Datenhaltung1()
        {
          
        }

        #region Profile Persistance
        public List<IProfile> ReadAllUsers()
        {
            var xmlReader = new ProfileXMLReader();
            return xmlReader.ReadAllUsers();
        }
        public void SaveProfile(IProfile profile)
        {
            var xmlWriter = new ProfileXMLWriter();
            xmlWriter.Save(profile);
        }
        public void RemoveProfile(int id)
        {
            var xmlWriter = new ProfileXMLWriter();
            xmlWriter.Remove(id);
        }
        #endregion //Profile Persistance

        #region Group Persistance
        public List<IGroup> ReadAllGroups()
        {
            var xmlArticleReader = new ArticleXMLReader();
            var xmlGroupReader = new GroupXMLReader(xmlArticleReader.ReadAllArticles());
            return xmlGroupReader.ReadAllGroups();
        }

        public void SaveGroup(IGroup group)
        {
            var xmlWriter = new GroupXMLWriter();
            xmlWriter.Save(group);
        }

        public void RemoveGroup(int id)
        {
            var xmlWriter = new GroupXMLWriter();
            xmlWriter.Remove(id);
        }
        #endregion //Group Persistance

        #region Article Persistance
        public List<IArticle> ReadAllArticles()
        {
            var xmlArticleReader = new ArticleXMLReader();
            return xmlArticleReader.ReadAllArticles();
        }
        public void SaveArticle(IArticle article)
        {
            var xmlWriter = new ArticleXMLWriter();
            xmlWriter.Save(article);
        }
        public void RemoveArticle(int id)
        {
            var xmlWriter = new ArticleXMLWriter();
            xmlWriter.Remove(id);
        }
        #endregion //Article Persistance

        #region Order Persistance
        public List<IOrder> ReadAllOrders()
        {
            var xmlReader = new OrderXMLReader();
            return xmlReader.ReadAllOrders();
        }
        public void SaveOrder(IOrder order)
        {
            var xmlWriter = new OrderXMLWriter();
            xmlWriter.Save(order);
        }
        public void RemoveOrder(IOrder order)
        {
            var xmlWriter = new OrderXMLWriter();
            xmlWriter.Remove(order);
        }
        #endregion //Order Persistance












    }
}
