﻿namespace DigiNote.Models
{
    using DigiNote.Interfaces;

    public class Fachkonzept2 : IFachkonzept
    {
        IDatenhaltung datenhaltung;

        public Fachkonzept2(IDatenhaltung datenhaltung)
        {
            this.datenhaltung = datenhaltung;
        }



        public Interfaces.Business.IProfile CurentUser
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool IsUserChanged
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool IsLoginSuccessfull
        {
            get { throw new System.NotImplementedException(); }
        }

        public void ValidateUser(string userName, string password)
        {
            throw new System.NotImplementedException();
        }

        public void PerformLogIn()
        {
            throw new System.NotImplementedException();
        }

        public void PerformLogOut()
        {
            throw new System.NotImplementedException();
        }

        public System.Collections.Generic.IEnumerable<Interfaces.Business.IProfile> GetProfiles()
        {
            throw new System.NotImplementedException();
        }

        public NewProfileValidationStatus ValidateNewPofile(string userName, string password, string confirmPassword)
        {
            throw new System.NotImplementedException();
        }

        public Interfaces.Business.IProfile CreateProfile()
        {
            throw new System.NotImplementedException();
        }

        public void EditProfile(Interfaces.Business.IProfile profile)
        {
            throw new System.NotImplementedException();
        }

        public bool CanDeleteProfile(Interfaces.Business.IProfile profile)
        {
            throw new System.NotImplementedException();
        }

        public void DeleteProfile(Interfaces.Business.IProfile profile)
        {
            throw new System.NotImplementedException();
        }

        public System.Collections.Generic.IEnumerable<Interfaces.Business.IGroup> GetGroups()
        {
            throw new System.NotImplementedException();
        }

        public NewItemValidationStatus ValidateNewGroup(string name)
        {
            throw new System.NotImplementedException();
        }

        public Interfaces.Business.IGroup CreateGroup(string name)
        {
            throw new System.NotImplementedException();
        }

        public void EditGroup(int id, string name)
        {
            throw new System.NotImplementedException();
        }

        public bool CanDeleteGroup(int id)
        {
            throw new System.NotImplementedException();
        }

        public void DeleteGroup(int id)
        {
            throw new System.NotImplementedException();
        }

        public void AddArticleToGroup(int groupId, int articleId)
        {
            throw new System.NotImplementedException();
        }

        public void RemouveArticleFromGroup(int groupId, int articleId)
        {
            throw new System.NotImplementedException();
        }

        public System.Collections.Generic.IEnumerable<Interfaces.Business.IArticle> GetArticlesFromGroup(int groupId)
        {
            throw new System.NotImplementedException();
        }

        public System.Collections.Generic.IEnumerable<Interfaces.Business.IArticle> GetArticles()
        {
            throw new System.NotImplementedException();
        }

        public NewItemValidationStatus ValidateNewArticle(string name)
        {
            throw new System.NotImplementedException();
        }

        public Interfaces.Business.IArticle CreateArticle(string name, double price)
        {
            throw new System.NotImplementedException();
        }

        public void EditArticle(int id, string name, double price)
        {
            throw new System.NotImplementedException();
        }

        public bool CanDeleteArticle(int id)
        {
            throw new System.NotImplementedException();
        }

        public void DeleteArticle(int id)
        {
            throw new System.NotImplementedException();
        }

        public System.Collections.Generic.IEnumerable<Interfaces.Business.IGroup> GetGroupsFromArticle(int articleId)
        {
            throw new System.NotImplementedException();
        }

        public System.Collections.Generic.IEnumerable<Interfaces.Business.IOrder> GetOrders()
        {
            throw new System.NotImplementedException();
        }

        public Interfaces.Business.IOrder CreateOrder(int articleId, int ammount, string comment)
        {
            throw new System.NotImplementedException();
        }

        public void PurchaseItem(int articleId)
        {
            throw new System.NotImplementedException();
        }


        public void EditProfile(int id, string userName, string firstName, string lastName, string password, System.Windows.Media.Imaging.BitmapSource image)
        {
            throw new System.NotImplementedException();
        }

        public bool CanDeleteProfile(int id)
        {
            throw new System.NotImplementedException();
        }

        public void DeleteProfile(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}
