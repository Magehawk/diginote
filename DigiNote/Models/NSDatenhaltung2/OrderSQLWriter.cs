﻿using DigiNote.Interfaces.Business;
using DigiNote.Models.Business;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DigiNote.Models.NSDatenhaltung2
{
    public class OrderSQLWriter
    {
        public void Save(IOrder order)
        {
            using (SqlConnection con = new SqlConnection(Datenhaltung2.CONSTRING))
            {
                con.Open();
                var SQL_String = String.Format("insert into dbo.DN_Bestellung values ({0},{1},{2},'{3}','{4}') ", order.ArticleId, order.ProfileId, order.Amount, order.Comment, order.CommentTimeStamp);
                SqlCommand cmd = new SqlCommand(SQL_String, con);
                cmd.ExecuteNonQuery();
            }
        }
        public void Remove(IOrder order)
        {
            using (SqlConnection con = new SqlConnection(Datenhaltung2.CONSTRING))
            {
                con.Open();
                var SQL_String = String.Format("DELETE From dbo.DN_Bestellung WHERE ArticleID = {0} and ProfilID={1} ", order.ArticleId, order.ProfileId);
                SqlCommand cmd = new SqlCommand(SQL_String, con);
                cmd.ExecuteNonQuery();
            }
        }


    }
}
