﻿using DigiNote.Interfaces.Business;
using DigiNote.Models.Business;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DigiNote.Models.NSDatenhaltung2
{
    public class OrderSQLReader
    {
        public List<IOrder> ReadAll()
        {
            List<IOrder> order = new List<IOrder>();
            using (SqlConnection con = new SqlConnection(Datenhaltung2.CONSTRING))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("Select * from dbo.DN_Bestellung", con);

                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
   
                    var ArticleID = int.Parse(reader["ArticleID"].ToString());//int.Parse(xmlProfile.Attribute("id").Value);
                    var ProfilID = int.Parse(reader["ProfileID"].ToString());//LazyEncryption.Decrypt(xmlProfile.Attribute("password").Value);
                    var Amount = int.Parse(reader["Amount"].ToString());//xmlProfile.Element(xmlns + "user").Value;
                    var Comment = reader["Comment"].ToString();//xmlProfile.Element(xmlns + "firstName").Value;
                    var CommentTimeStamp = reader["CommentTimeStamp"].ToString();//xmlProfile.Element(xmlns + "lastName").Value;
                    var newOrder = new Order(ProfilID,ArticleID,Amount);
                    newOrder.Comment= Comment;
                    newOrder.CommentTimeStamp= DateTime.Parse(CommentTimeStamp);
                    order.Add(newOrder);
                }
            }
            return order;
        }

    }
}
