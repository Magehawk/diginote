﻿using DigiNote.Interfaces.Business;
using DigiNote.Models.Business;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;

namespace DigiNote.Models.NSDatenhaltung2
{
    public class ProfileSQLReader
    {
        public List<IProfile> ReadAll()
        {
            List<IProfile> profile = new List<IProfile>();
            using (SqlConnection con = new SqlConnection(Datenhaltung2.CONSTRING))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("Select * from dbo.DN_Profil", con);

                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var newProfile = new Profile();
                    newProfile.ID = int.Parse(reader["ID"].ToString());//int.Parse(xmlProfile.Attribute("id").Value);
                    newProfile.Password = reader["Pass"].ToString();//LazyEncryption.Decrypt(xmlProfile.Attribute("password").Value);
                    newProfile.UserName = reader["NickName"].ToString();//xmlProfile.Element(xmlns + "user").Value;
                    newProfile.FirstName = reader["FirstName"].ToString();//xmlProfile.Element(xmlns + "firstName").Value;
                    newProfile.LastName = reader["LastName"].ToString();//xmlProfile.Element(xmlns + "lastName").Value;
                    newProfile.Image = Base64ToImage(reader["BitMap"].ToString());
                    profile.Add(newProfile);
                }
            }
            return profile;
        }

        private BitmapSource Base64ToImage(string base64)
        {
            byte[] bytes = Convert.FromBase64String(base64);
            BitmapImage bmp = new BitmapImage();
            if (bytes.Length != 0)
            {
                bmp.BeginInit();
                bmp.StreamSource = new MemoryStream(bytes);
                bmp.EndInit();
            }

            return bmp;
        }

    }
}
