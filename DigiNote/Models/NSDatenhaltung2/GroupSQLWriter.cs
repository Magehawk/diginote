﻿using DigiNote.Interfaces.Business;
using DigiNote.Models.Business;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DigiNote.Models.NSDatenhaltung2
{
    public class GroupSQLWriter
    {
        public void Save(IGroup group)
        {
            using (SqlConnection con = new SqlConnection(Datenhaltung2.CONSTRING))
            {
                con.Open();
                var SQL_String = String.Format("Delete From dbo.DN_GroupArtikel WHERE GroupID = {0}", group.ID);
                SqlCommand cmd = new SqlCommand(SQL_String, con);
                cmd.ExecuteNonQuery();
                SQL_String = String.Format("Delete From dbo.DN_Group WHERE ID = {0}", group.ID);
                cmd = new SqlCommand(SQL_String, con);
                cmd.ExecuteNonQuery();
                SQL_String = String.Format("insert into dbo.DN_Group values ({0},'{1}') ", group.ID, group.Name);
                cmd = new SqlCommand(SQL_String, con);
                cmd.ExecuteNonQuery();
                foreach (int ID in group.Articles)
                {
                    SQL_String = String.Format("insert into dbo.DN_GroupArtikel values ({0},'{1}') ", ID, group.ID);
                    cmd = new SqlCommand(SQL_String, con);
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public void Remove(int group)
        {
            using (SqlConnection con = new SqlConnection(Datenhaltung2.CONSTRING))
            {
                con.Open();
                var SQL_String = String.Format("DELETE From dbo.DN_Group WHERE ID = {0} ", group);
                SqlCommand cmd = new SqlCommand(SQL_String, con);
                cmd.ExecuteNonQuery();
            }
        }


    }
}
