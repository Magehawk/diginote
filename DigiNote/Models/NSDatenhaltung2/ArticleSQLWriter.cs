﻿using DigiNote.Interfaces.Business;
using DigiNote.Models.Business;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DigiNote.Models.NSDatenhaltung2
{
    public class ArticleSQlWriter
    {
        public void Save(IArticle article)
        {
            using (SqlConnection con = new SqlConnection(Datenhaltung2.CONSTRING))
            {
                con.Open();
                var SQL_String = String.Format("insert into dbo.DN_Artikel values ({0},'{1}','{2}') ", article.ID, article.Name, article.Preis);
                SqlCommand cmd = new SqlCommand(SQL_String, con);
                cmd.ExecuteNonQuery();
            }
        }
        public void Remove(int article)
        {
            using (SqlConnection con = new SqlConnection(Datenhaltung2.CONSTRING))
            {
                con.Open();
                var SQL_String = String.Format("DELETE From dbo.DN_Artikel WHERE ID = {0}", article);
                SqlCommand cmd = new SqlCommand(SQL_String, con);
                cmd.ExecuteNonQuery();
            }
        }

    }
}
