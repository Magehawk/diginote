﻿using DigiNote.Interfaces.Business;
using DigiNote.Models.Business;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;

namespace DigiNote.Models.NSDatenhaltung2
{
    public class ProfileSQLWriter
    {
        public void Save(IProfile profile)
        {
            using (SqlConnection con = new SqlConnection(Datenhaltung2.CONSTRING))
            {
                
                
                var SQL_String = String.Format("insert into dbo.DN_Profil values ({0},'{1}','{2}','{3}','{4}','{5}') ", profile.ID , profile.LastName , profile.FirstName , profile.UserName , profile.Password , ImageToBase64( profile.Image) );
                using (SqlCommand cmd = new SqlCommand(SQL_String, con))
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        private string ImageToBase64(BitmapSource bit)
        {
            try
            {
                var encoder = new PngBitmapEncoder();
                var frame = BitmapFrame.Create(bit);
                encoder.Frames.Add(frame);
                using (var stream = new MemoryStream())
                {
                    encoder.Save(stream);
                    return Convert.ToBase64String(stream.ToArray());
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public void Remove(int profile)
        {
            using (SqlConnection con = new SqlConnection(Datenhaltung2.CONSTRING))
            {
                con.Open();
                var SQL_String = String.Format("DELETE From dbo.DN_Profil WHERE ID = {0}", profile);
                SqlCommand cmd = new SqlCommand(SQL_String, con);
                cmd.ExecuteNonQuery();
            }
        }
    }
}
