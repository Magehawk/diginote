﻿using DigiNote.Interfaces.Business;
using DigiNote.Models.Business;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DigiNote.Models.NSDatenhaltung2
{
    public class ArticleSQLReader
    {
        public List<IArticle> ReadAll()
        {
            List<IArticle> article = new List<IArticle>();
            
            using (SqlConnection con = new SqlConnection(Datenhaltung2.CONSTRING))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("Select * from dbo.DN_Artikel", con);

                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {                    
                    var ID = int.Parse(reader["ID"].ToString());//int.Parse(xmlProfile.Attribute("id").Value);
                    var Name = reader["Name"].ToString();//xmlProfile.Element(xmlns + "user").Value;
                    var newArticle = new Article(ID,Name);
                    article.Add(newArticle);
                }
            }
            return article;
        }

    }
}
