﻿using DigiNote.Interfaces.Business;
using DigiNote.Models.Business;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DigiNote.Models.NSDatenhaltung2
{
    public class GroupSQLReader
    {
        public List<IGroup> ReadAll()
        {
            List<IGroup> group = new List<IGroup>();
            using (SqlConnection con = new SqlConnection(Datenhaltung2.CONSTRING))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("Select * from dbo.DN_Group", con);

                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {           
                    var ID = int.Parse(reader["ID"].ToString());//int.Parse(xmlProfile.Attribute("id").Value);
                    var Name = reader["Name"].ToString();//LazyEncryption.Decrypt(xmlProfile.Attribute("password").Value);
                    var newGroup = new Group(ID,Name);
                    group.Add(newGroup);
                }
            }
            return group;
        }

    }
}
