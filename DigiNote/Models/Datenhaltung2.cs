﻿namespace DigiNote.Models
{

    using DigiNote.Interfaces;
    using DigiNote.Interfaces.Business;
    using System.Data.SqlClient;
    using DigiNote.Models.NSDatenhaltung2;
    public class Datenhaltung2 : IDatenhaltung
    {
        public static readonly string CONSTRING = @"Data Source=TEST-PC\SQLEXPRESS;Initial Catalog=DigiDB;Integrated Security=True;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False";//"data source=.; database = DigiDb;";

        public Datenhaltung2()
        {

        }


        public System.Collections.Generic.List<IProfile> ReadAllUsers()
        {
            var reader = new ProfileSQLReader();
            return reader.ReadAll();

        }

        public void SaveProfile(IProfile profile)
        {
            var writer = new ProfileSQLWriter();
            writer.Save(profile);
        }

        public void RemoveProfile(int id)
        {
            var writer = new ProfileSQLWriter();
            writer.Remove(id);
        }

        public System.Collections.Generic.List<IGroup> ReadAllGroups()
        {
            var reader = new GroupSQLReader();
            return reader.ReadAll();
        }

        public void SaveGroup(IGroup group)
        {
            var writer = new GroupSQLWriter();
            writer.Save(group);
        }

        public void RemoveGroup(int id)
        {
            var writer = new GroupSQLWriter();
            writer.Remove(id);
        }

        public System.Collections.Generic.List<IArticle> ReadAllArticles()
        {
            var reader = new ArticleSQLReader();
            return reader.ReadAll();
        }

        public void SaveArticle(IArticle article)
        {
            var writer = new ArticleSQlWriter();
            writer.Save(article);
        }

        public void RemoveArticle(int id)
        {
            var writer = new ArticleSQlWriter();
            writer.Remove(id);
        }

        public System.Collections.Generic.List<IOrder> ReadAllOrders()
        {
            var reader = new OrderSQLReader();
            return reader.ReadAll();
        }

        public void SaveOrder(IOrder order)
        {
            var writer = new OrderSQLWriter();
            writer.Save(order);
        }

        public void RemoveOrder(IOrder order)
        {
            var writer = new OrderSQLWriter();
            writer.Remove(order);
        }
    }
}
