﻿namespace DigiNote.Models.Business
{
    using DigiNote.Interfaces.Business;
    using System;

    public class Order : IOrder
    {
        #region Fields
        private int profile;
        private int article;
        private int ammount;
        private string comment;
        private DateTime commentTimeStamp;
        #endregion //Fields

        #region Constructors
        public Order(int profileId, int articleId, int ammount)
        {
            this.profile = profileId;
            this.article = articleId;
            this.ammount = ammount;
            Comment = string.Empty;
            CommentTimeStamp = DateTime.Now;
        }
        #endregion //Constructors

        #region Properties
        public int ProfileId { get { return profile; } }
        
        public int ArticleId
        {
            get { return article; }
        }

        public int Amount
        {
            get { return ammount; }
        }

        public string Comment
        {
            get { return comment; }
            set { comment = value; }
        }

        public System.DateTime CommentTimeStamp
        {
            get { return commentTimeStamp; }
            set { commentTimeStamp = value; }
        }
        #endregion //Properties
    }
}
