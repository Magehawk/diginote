﻿namespace DigiNote.Models.Business
{
    using DigiNote.Interfaces.Business;
    using System.Collections.Generic;

    public class Group : IGroup
    {
        #region Fields
        private int id;
        private string name;
        private readonly List<int> articles;
        #endregion //Fields

        #region Constructors
        public Group(int id, string name)
        {
            ID = id;
            Name = name;
            articles = new List<int>();
        }
        #endregion //Constructors

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public List<int> Articles
        {
            get { return articles; }
        }
    }
}
