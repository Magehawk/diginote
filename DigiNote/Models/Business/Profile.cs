﻿namespace DigiNote.Models.Business
{
    using DigiNote.Interfaces.Business;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.IO;
    using System;

    public class Profile : IProfile
    {
        #region Fields
        private int id;
        private string userName;
        private string firstName;
        private string lastName;
        private string password;
        private BitmapSource image;
        #endregion //Fields

        #region Constructors
        public Profile()
        {
            ID = -1;
            UserName = "root";
            FirstName = string.Empty;
            LastName = string.Empty;
            Password = "diginote";
            var image = new BitmapImage();
            image.BeginInit();
            image.UriSource = new System.Uri(@"\Resources\img\Dummy.png", System.UriKind.Relative);
            image.EndInit();
            Image = image;
        }
        #endregion //Constructors

        #region Properties
        public int ID
        {
            get { return id; }
            set { id = value; }
        }
        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }

        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        public BitmapSource Image
        {
            get { return image; }
            set { image = value; }
        }
        #endregion //Properties

        #region Methods
        #endregion //Methods
    }
}
