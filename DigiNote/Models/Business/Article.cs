﻿namespace DigiNote.Models.Business
{
    using DigiNote.Interfaces.Business;

    public class Article : IArticle
    {
        #region Fiels
        private int id;
        private string name;
        private double preis;
        #endregion //Fields

        #region Constructors
        public Article(int id, string name)
        {
            ID = id;
            Name = name;
            Preis = 0;
        }
        #endregion //Constructors

        #region Properties
        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public double Preis
        {
            get { return preis; }
            set { preis = value; }
        }
        #endregion //Properties
    }
}
