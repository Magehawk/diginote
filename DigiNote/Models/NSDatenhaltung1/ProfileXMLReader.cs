﻿namespace DigiNote.Models.NSDatenhaltung1
{
    using DigiNote.Interfaces.Business;
    using System.Windows.Media.Imaging;
    using System.IO;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;
    using DigiNote.Models.Business;

    public class ProfileXMLReader
    {
        private XNamespace xmlns;
        private XNamespace xsi;
        private XDocument document;
        private string filePath;

        public ProfileXMLReader()
        {
            xmlns = XNamespace.Get("http://profile.diginote.de/xmlns");
            xsi = XNamespace.Get("http://w3.org/2001/XMLSchema-instance");
            filePath = AppDomain.CurrentDomain.BaseDirectory + @"Save\Profiles.xml";
        }

        public List<IProfile> ReadAllUsers()
        {
            var profiles= new List<IProfile>();

            FileInfo saveFile = new FileInfo(filePath);
            if (saveFile.Exists)
            {
                document = XDocument.Load(filePath);
                IEnumerable<XElement> xmlProfiles = document.Element(xmlns + "Profiles").Elements(xmlns + "Profile");
                foreach (var xmlProfile in xmlProfiles) 
                {
                    var newProfile = new Profile();
                    newProfile.ID = int.Parse(xmlProfile.Attribute("id").Value);
                    newProfile.Password = LazyEncryption.Decrypt(xmlProfile.Attribute("password").Value);
                    newProfile.UserName = xmlProfile.Element(xmlns + "user").Value;
                    newProfile.FirstName = xmlProfile.Element(xmlns + "firstName").Value;
                    newProfile.LastName = xmlProfile.Element(xmlns + "lastName").Value;

                    newProfile.Image = Base64ToImage(xmlProfile.Element(xmlns + "image").Value);
                    profiles.Add(newProfile);
                }
            }

            return profiles;
        }

        private BitmapSource Base64ToImage(string base64)
        {
            byte[] bytes = Convert.FromBase64String(base64);
            BitmapImage bmp = new BitmapImage();
            if (bytes.Length != 0)
            {
                bmp.BeginInit();
                bmp.StreamSource = new MemoryStream(bytes);
                bmp.EndInit();
            }
            
            return bmp;
        }
    }
}
