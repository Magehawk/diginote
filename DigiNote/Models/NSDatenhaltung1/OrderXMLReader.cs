﻿namespace DigiNote.Models.NSDatenhaltung1
{
    using DigiNote.Interfaces.Business;
    using System.Windows.Media.Imaging;
    using System.IO;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;
    using DigiNote.Models.Business;

    public class OrderXMLReader
    {
        private XNamespace xmlns;
        private XNamespace xsi;
        private XDocument document;
        private string filePath;

        public OrderXMLReader()
        {
            xmlns = XNamespace.Get("http://orders.diginote.de/xmlns");
            xsi = XNamespace.Get("http://w3.org/2001/XMLSchema-instance");
            filePath = AppDomain.CurrentDomain.BaseDirectory + @"Save\Orders.xml";
        }

        public List<IOrder> ReadAllOrders() //TODO Compare xml keys
        {
            var groups = new List<IOrder>();

            FileInfo saveFile = new FileInfo(filePath);
            if (saveFile.Exists)
            {
                document = XDocument.Load(filePath);
                IEnumerable<XElement> xmlOrders = document.Element(xmlns + "Orders").Elements(xmlns + "Order");
                foreach (var xmlOrder in xmlOrders)
                {
                    var profileId = int.Parse(xmlOrder.Attribute("profile").Value);
                    var articleId = int.Parse(xmlOrder.Attribute("article").Value);
                    var amount = int.Parse(xmlOrder.Attribute("amount").Value);
                    var newOrder = new Order(profileId, articleId,amount);
                    newOrder.Comment = xmlOrder.Value;
                    newOrder.CommentTimeStamp = DateTime.Parse(xmlOrder.Attribute("time").Value);
                    groups.Add(newOrder);
                }
            }

            return groups;
        }
    }
}
