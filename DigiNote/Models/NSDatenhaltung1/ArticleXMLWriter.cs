﻿namespace DigiNote.Models.NSDatenhaltung1
{
    using DigiNote.Interfaces.Business;
    using System.Windows.Media.Imaging;
    using System.IO;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;

    public class ArticleXMLWriter
    {
        private XNamespace xmlns;
        private XNamespace xsi;
        private XDocument document;
        private string filePath;

        public ArticleXMLWriter()
        {
            xmlns = XNamespace.Get("http://article.diginote.de/xmlns");
            xsi = XNamespace.Get("http://w3.org/2001/XMLSchema-instance");
            filePath = AppDomain.CurrentDomain.BaseDirectory + @"Save\Articles.xml";
        }

        public void Save(IArticle article)
        {
            FileInfo saveFile = new FileInfo(filePath);
            if (!saveFile.Directory.Exists)
                saveFile.Directory.Create();
            if (saveFile.Exists)
            {
                document = XDocument.Load(filePath);
            }
            else
            {
                PrepareDocument();
            }
            AddArticle(article);
            document.Save(filePath);
        }

        private void PrepareDocument()
        {
            document = new XDocument(
                new XDeclaration("1.0", "UTF-8", "no"),
                new XElement(xmlns + "Articles",
                    new XAttribute("xmlns", xmlns.NamespaceName),
                    new XAttribute(XNamespace.Xmlns + "xsi", xsi.NamespaceName)));
        }

        private void AddArticle(IArticle article)
        {
            XElement xArticle = GetArticleElement(article);
            var newArticle = new XElement(xmlns + "Article",
                new XAttribute("id", article.ID),
                new XAttribute("price",article.Preis),
                new XElement(xmlns + "name", new XCData(article.Name)));
            xArticle.ReplaceWith(newArticle);
        }

        private XElement GetArticleElement(IArticle article)
        {
            IEnumerable<XElement> articles = document.Element(xmlns + "Articles").Elements(xmlns + "Article");
            if (articles.Count() > 0)
            {
                IEnumerable<XElement> filteredArticles = articles.Where(x => x.Attribute("id").Value == article.ID.ToString());
                if (filteredArticles.Count() > 0)
                    return filteredArticles.First();
            }

            XElement newArticle = new XElement(xmlns + "Article");
            document.Element(xmlns + "Articles").Add(newArticle);
            return newArticle;
        }

        internal void Remove(int id)
        {
            FileInfo saveFile = new FileInfo(filePath);
            if (!saveFile.Directory.Exists)
                saveFile.Directory.Create();
            if (saveFile.Exists)
            {
                document = XDocument.Load(filePath);
                document.Element(xmlns + "Articles").Elements(xmlns + "Article")
                    .Where(x => x.Attribute("id").Value == id.ToString()).First().Remove();

            }
            document.Save(filePath);
        }
    }
}
