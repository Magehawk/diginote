﻿namespace DigiNote.Models.NSDatenhaltung1
{
    using DigiNote.Interfaces.Business;
    using System.Windows.Media.Imaging;
    using System.IO;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;
    using DigiNote.Models.Business;

    public class ArticleXMLReader
    {
        private XNamespace xmlns;
        private XNamespace xsi;
        private XDocument document;
        private string filePath;

        public ArticleXMLReader()
        {
            xmlns = XNamespace.Get("http://article.diginote.de/xmlns");
            xsi = XNamespace.Get("http://w3.org/2001/XMLSchema-instance");
            filePath = AppDomain.CurrentDomain.BaseDirectory + @"Save\Articles.xml";
        }

        public List<IArticle> ReadAllArticles() // TODO Check for lost input
        {
            var articles = new List<IArticle>();

            FileInfo saveFile = new FileInfo(filePath);
            if (saveFile.Exists)
            {
                document = XDocument.Load(filePath);
                IEnumerable<XElement> xmlArticles = document.Element(xmlns + "Articles").Elements(xmlns + "Article");
                foreach (var xmlProfile in xmlArticles)
                {
                    
                    int id = int.Parse(xmlProfile.Attribute("id").Value);
                    string name = xmlProfile.Element(xmlns + "name").Value;
                    var newArticle = new Article(id,name);
                    newArticle.Preis = (double)xmlProfile.Attribute("price");
                    articles.Add(newArticle);
                }
            }

            return articles;
        }
    }
}
