﻿namespace DigiNote.Models.NSDatenhaltung1
{
    using DigiNote.Interfaces.Business;
    using System.Windows.Media.Imaging;
    using System.IO;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;

    public class ProfileXMLWriter
    {
        private XNamespace xmlns;
        private XNamespace xsi;
        private XDocument document;
        private string filePath;

        public ProfileXMLWriter()
        {
            xmlns = XNamespace.Get("http://profile.diginote.de/xmlns");
            xsi = XNamespace.Get("http://w3.org/2001/XMLSchema-instance");
            filePath = AppDomain.CurrentDomain.BaseDirectory + @"Save\Profiles.xml";
        }

        public void Save(IProfile profile)
        {
            FileInfo saveFile = new FileInfo(filePath);
            if (!saveFile.Directory.Exists)
                saveFile.Directory.Create();
            if (saveFile.Exists)
            {
                document = XDocument.Load(filePath);
            }
            else
            {
                PrepareDocument();
            }
            AddProfile(profile);
            document.Save(filePath);
        }

        private void PrepareDocument()
        {
            document= new XDocument(
                new XDeclaration("1.0","UTF-8","no"),
                new XElement(xmlns+"Profiles",
                    new XAttribute("xmlns",xmlns.NamespaceName),
                    new XAttribute(XNamespace.Xmlns+"xsi",xsi.NamespaceName)));
        }

        private void AddProfile(IProfile profile)
        {
            XElement xProfile = GetProfileElement(profile);
            var newProfile=new XElement(xmlns+"Profile",
                new XAttribute("id",profile.ID),
                new XAttribute("password", LazyEncryption.Crypt(profile.Password)), //Verschlüsseltes Passwort
                new XElement(xmlns+"user",new XCData(profile.UserName)),
                new XElement(xmlns+"firstName",new XCData(profile.FirstName)),
                new XElement(xmlns+"lastName",new XCData(profile.LastName)),
                new XElement(xmlns+"image",new XCData(ImageToBase64(profile.Image))));
            xProfile.ReplaceWith(newProfile);
        }

        private XElement GetProfileElement(IProfile profile)
        {
            IEnumerable<XElement> profiles = document.Element(xmlns+"Profiles").Elements(xmlns+"Profile");
            if(profiles.Count()>0)
            {
                IEnumerable<XElement> filteredProfiles = profiles.Where(x=>x.Attribute("id").Value==profile.ID.ToString());
                if (filteredProfiles.Count() > 0)
                    return filteredProfiles.First();
            }

            XElement newProfile= new XElement(xmlns+"Profile");
            document.Element(xmlns+"Profiles").Add(newProfile);
            return newProfile;
        }

        private string ImageToBase64(BitmapSource bit)
        {
            try
            {
                var encoder = new PngBitmapEncoder();
                var frame = BitmapFrame.Create(bit);
                encoder.Frames.Add(frame);
                using (var stream = new MemoryStream())
                {
                    encoder.Save(stream);
                    return Convert.ToBase64String(stream.ToArray());
                }
            }
            catch(Exception ex)
            {
                return "";
            }
        }

        internal void Remove(int id)
        {
            FileInfo saveFile = new FileInfo(filePath);
            if (!saveFile.Directory.Exists)
                saveFile.Directory.Create();
            if (saveFile.Exists)
            {
                document = XDocument.Load(filePath);
                document.Element(xmlns + "Profiles").Elements(xmlns + "Profile")
                    .Where(x => x.Attribute("id").Value == id.ToString()).First().Remove();

            }
            document.Save(filePath);
        }
    }
}
