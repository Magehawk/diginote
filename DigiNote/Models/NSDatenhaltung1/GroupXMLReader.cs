﻿namespace DigiNote.Models.NSDatenhaltung1
{
    using DigiNote.Interfaces.Business;
    using System.Windows.Media.Imaging;
    using System.IO;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;
    using DigiNote.Models.Business;

    public class GroupXMLReader
    {
        private XNamespace xmlns;
        private XNamespace xsi;
        private XDocument document;
        private List<IArticle> articles;
        private string filePath;

        public GroupXMLReader(List<IArticle> articles)
        {
            xmlns = XNamespace.Get("http://group.diginote.de/xmlns");
            xsi = XNamespace.Get("http://w3.org/2001/XMLSchema-instance");
            filePath = AppDomain.CurrentDomain.BaseDirectory + @"Save\Groups.xml";
            this.articles = articles;
        }

        public List<IGroup> ReadAllGroups() 
        {
            var groups = new List<IGroup>();

            FileInfo saveFile = new FileInfo(filePath);
            if (saveFile.Exists)
            {
                document = XDocument.Load(filePath);
                IEnumerable<XElement> xmlGroups = document.Element(xmlns + "Groups").Elements(xmlns + "Group");
                foreach (var xmlGroup in xmlGroups)
                {
                    var ID = int.Parse(xmlGroup.Attribute("id").Value);
                    var Name = xmlGroup.Element(xmlns + "name").Value;
                    var newGroup = new Group(ID, Name);
                    readArticles(xmlGroup, newGroup);
                    groups.Add(newGroup);
                }
            }

            return groups;
        }

        private void readArticles(XElement xmlGroup, IGroup group)
        {
            var xmlArticles = xmlGroup.Elements(xmlns + "article");
            foreach (var xmlArticle in xmlArticles)
            {
                group.Articles.Add(int.Parse(xmlArticle.Attribute("id").Value));
            }
        }
    }
}
