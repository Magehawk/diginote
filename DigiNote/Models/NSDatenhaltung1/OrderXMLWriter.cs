﻿namespace DigiNote.Models.NSDatenhaltung1
{
    using DigiNote.Interfaces.Business;
    using System.Windows.Media.Imaging;
    using System.IO;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;

    public class OrderXMLWriter
    {
        private XNamespace xmlns;
        private XNamespace xsi;
        private XDocument document;
        private string filePath;

        public OrderXMLWriter()
        {
            xmlns = XNamespace.Get("http://orders.diginote.de/xmlns");
            xsi = XNamespace.Get("http://w3.org/2001/XMLSchema-instance");
            filePath = AppDomain.CurrentDomain.BaseDirectory + @"Save\Orders.xml";
        }

        public void Save(IOrder order)
        {
            FileInfo saveFile = new FileInfo(filePath);
            if (!saveFile.Directory.Exists)
                saveFile.Directory.Create();
            if (saveFile.Exists)
            {
                document = XDocument.Load(filePath);
            }
            else
            {
                PrepareDocument();
            }
            AddOrder(order);
            document.Save(filePath);
        }

        private void PrepareDocument()
        {
            document = new XDocument(
                new XDeclaration("1.0", "UTF-8", "no"),
                new XElement(xmlns + "Orders",
                    new XAttribute("xmlns", xmlns.NamespaceName),
                    new XAttribute(XNamespace.Xmlns + "xsi", xsi.NamespaceName)));
        }

        private void AddOrder(IOrder order)
        {
            XElement xOrder = GetOrderElement(order);
            var newOrder = new XElement(xmlns + "Order",
                new XAttribute("profile", order.ProfileId),
                new XAttribute("article", order.ArticleId),
                new XAttribute("amount", order.Amount),
                new XAttribute("time", order.CommentTimeStamp),
                new XCData(order.Comment));

            xOrder.ReplaceWith(newOrder);
        }

        private XElement GetOrderElement(IOrder order)
        {
            IEnumerable<XElement> orders = document.Element(xmlns + "Orders").Elements(xmlns + "Order");
            if (orders.Count() > 0)
            {
                IEnumerable<XElement> filteredOrders = orders.Where(x => x.Attribute("profile").Value == order.ProfileId.ToString()
                                                                      && x.Attribute("article").Value == order.ArticleId.ToString());
                if (filteredOrders.Count() > 0)
                    return filteredOrders.First();
            }

            XElement newOrder = new XElement(xmlns + "Order");
            document.Element(xmlns + "Orders").Add(newOrder);
            return newOrder;
        }

        internal void Remove(IOrder order)// TODO OrderXMLWriter remove
        {
            throw new NotImplementedException();
        }
    }
}
