﻿namespace DigiNote.Models.NSDatenhaltung1
{
    using DigiNote.Interfaces.Business;
    using System.Windows.Media.Imaging;
    using System.IO;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;

    public class GroupXMLWriter
    {
        private XNamespace xmlns;
        private XNamespace xsi;
        private XDocument document;
        private string filePath;

        public GroupXMLWriter()
        {
            xmlns = XNamespace.Get("http://group.diginote.de/xmlns");
            xsi = XNamespace.Get("http://w3.org/2001/XMLSchema-instance");
            filePath = AppDomain.CurrentDomain.BaseDirectory + @"Save\Groups.xml";
        }

        public void Save(IGroup group)
        {
            FileInfo saveFile = new FileInfo(filePath);
            if (!saveFile.Directory.Exists)
                saveFile.Directory.Create();
            if (saveFile.Exists)
            {
                document = XDocument.Load(filePath);
            }
            else
            {
                PrepareDocument();
            }
            AddGroup(group);
            document.Save(filePath);
        }

        private void PrepareDocument()
        {
            document = new XDocument(
                new XDeclaration("1.0", "UTF-8", "no"),
                new XElement(xmlns + "Groups",
                    new XAttribute("xmlns", xmlns.NamespaceName),
                    new XAttribute(XNamespace.Xmlns + "xsi", xsi.NamespaceName)));
        }

        private void AddGroup(IGroup group)
        {
            XElement xGroup = GetGroupElement(group);
            var newGroup = new XElement(xmlns + "Group",
                new XAttribute("id", group.ID),
                new XElement(xmlns + "name", new XCData(group.Name)));
            foreach (var article in group.Articles)
                newGroup.Add(new XElement(xmlns + "article", new XAttribute("id", article)));
            
            xGroup.ReplaceWith(newGroup);
        }

        private XElement GetGroupElement(IGroup group)
        {
            IEnumerable<XElement> groups = document.Element(xmlns + "Groups").Elements(xmlns + "Group");
            if (groups.Count() > 0)
            {
                IEnumerable<XElement> filteredGroups = groups.Where(x => x.Attribute("id").Value == group.ID.ToString());
                if (filteredGroups.Count() > 0)
                    return filteredGroups.First();
            }

            XElement newGroup = new XElement(xmlns + "Group");
            document.Element(xmlns + "Groups").Add(newGroup);
            return newGroup;
        }

        internal void Remove(int id) //TODO GroupXMLWriter Remove
        {
            throw new NotImplementedException();
        }
    }
}
