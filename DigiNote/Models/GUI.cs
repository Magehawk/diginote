﻿namespace DigiNote.Models
{
    using DigiNote.Interfaces;

    public class GUI
    {
        private IFachkonzept fachkonzept;
        
        public GUI(IFachkonzept fachkonzept)
        {
            this.fachkonzept = fachkonzept;
            do
            {
                var MainWindow = new Views.MainWindow();
                MainWindow.SizeToContent = System.Windows.SizeToContent.WidthAndHeight;
                MainWindow.ResizeMode = System.Windows.ResizeMode.NoResize;
                MainWindow.DataContext = new ViewModels.LoginVM(fachkonzept, MainWindow);
                MainWindow.ShowDialog();

                if (fachkonzept.IsLoginSuccessfull)
                {
                    MainWindow = new Views.MainWindow();
                    MainWindow.SizeToContent = System.Windows.SizeToContent.Manual;
                    MainWindow.ResizeMode = System.Windows.ResizeMode.CanResize;
                    MainWindow.Width = 500;
                    MainWindow.Height = 400;
                    var mainVM = new ViewModels.MainVM(fachkonzept, MainWindow);
                    MainWindow.DataContext = mainVM;
                    MainWindow.ShowDialog();
                }
            } while (fachkonzept.IsUserChanged);
        }
    }
}
