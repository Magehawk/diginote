﻿namespace DigiNote.Models.NSTui
{
    using System;
    using DigiNote.Interfaces;
    using System.Collections.Generic;
    using System.Linq;

    class OrderManager
    {
        private IFachkonzept fachkonzept;
        private List<OrderTVM> orders;

        public OrderManager(IFachkonzept fachkonzept, List<OrderTVM> orders)
        {
            this.fachkonzept = fachkonzept;
            this.orders = orders;
        }

        private void PrintMenu()
        {
            Console.WriteLine("####   Einkaufszettel   ####");
            Console.WriteLine("Einkaufszettel anzeigen  (1)"); // einschließlich Gesamtwert
            Console.WriteLine("Warengesamtwert anzeigen (2)"); // nur Betrag
            Console.WriteLine("----------------------------");
            Console.WriteLine("Bestellung Aufgeben      (3)");
            Console.WriteLine("Einkauf eintragen        (4)");
            Console.WriteLine("----------------------------");
            Console.WriteLine("Zurück                   (x)");
        }

        public void Show()
        {
            bool exit = false;
            do
            {
                Console.Clear();
                PrintMenu();
                var key = Console.ReadKey();
                if (key.Key == ConsoleKey.D1)//Einkaufszettel anzeigen 
                {
                    Console.WriteLine();
                    PrintOrders(this.orders, this.fachkonzept);
                    Console.ReadKey();
                }
                if (key.Key == ConsoleKey.D2)//Warengesamtwert anzeigen	
                {
                    ShowSum();
                }
                if (key.Key == ConsoleKey.D3)//Bestellung Aufgeben    
                {
                    DoOrder();
                }
                if (key.Key == ConsoleKey.D4)//Einkauf eintragen   
                {
                    Checkout();
                }
                if (key.Key == ConsoleKey.X)//zurück 
                {
                    exit = true;
                }
            } while (!exit);
        }

        public static void PrintOrders(List<OrderTVM> orders, IFachkonzept fachkonzept)
        {
            orders.Clear();
            var articles = fachkonzept.GetArticles();
            foreach (var order in fachkonzept.GetOrders())
            {
                var orderVM = orders.Where(x => x.ArticleId == order.ArticleId);
                if (orderVM.Count() != 0)
                {
                    orderVM.First().Amount += order.Amount;
                }
                else
                {
                    var article = articles.Where(x => x.ID == order.ArticleId).First();
                    var newOrderVM = new OrderTVM()
                    {
                        ArticleId = article.ID,
                        Name = article.Name,
                        Price = article.Preis,
                        Amount = order.Amount
                    };
                    orders.Add(newOrderVM);
                }
            }

            int productLength = orders.Count > 0 ? orders.Select(x => x.Name.Length).Max() : 0;

            productLength = (productLength > 9 ? productLength : 9);
            string TEMPLATE = "|{0,-4}|{1,-8}|{2,-" + productLength + "}|{3,20}|{4,15}|";
            string header = String.Format(TEMPLATE, " Nr ", " Anzahl ", " Produkt ", " Grundpreis in Euro ", " Summe in Euro ");
            Console.WriteLine(header);
            Console.WriteLine(String.Format("|{0}+{1}+{2}+{3}+{4}|", "----", new String('-', 8), new String('-', productLength), new String('-', 20), new String('-', 15)));
            var sum = 0.0;
            foreach (var order in orders)
            {
                var subSum = (order.Price * order.Amount);
                sum += subSum;
                Console.WriteLine(String.Format(TEMPLATE, orders.IndexOf(order) + 1, order.Amount, order.Name, order.Price, subSum));
            }
            Console.WriteLine("\nGesamtbestrag:" + sum + " Euro\n");

        }


        private void ShowSum()
        {
            double sum = this.orders.Select(x => x.Price * x.Amount).Sum();
            Console.Write("\nGesamtsumme: " + sum + " Euro");
            Console.ReadKey();
        }

        public static void PrintComments(IFachkonzept fachkonzept)
        {
            var orders = fachkonzept.GetOrders();
            if (orders.Select(x => !String.IsNullOrWhiteSpace(x.Comment)).Count() > 0)
            {
                Console.WriteLine("## Hinweis ##");
                var articles = fachkonzept.GetArticles();
                var profiles = fachkonzept.GetProfiles();
                var orderComments = from order in orders
                                    join article in articles on order.ArticleId equals article.ID
                                    select new
                                    {
                                        Time = order.CommentTimeStamp,
                                        ProfileId = order.ProfileId,
                                        Comment = order.Comment,
                                        Article = article.Name
                                    };
                foreach (var order in orderComments)
                {
                    if (!String.IsNullOrWhiteSpace(order.Comment))
                    {
                        string user = "";
                        if (order.ProfileId == -1)
                            user = "root";
                        else
                            user = profiles.Where(x => x.ID == order.ProfileId).First().UserName;
                        Console.WriteLine(String.Format("{0} - {1} : {2}\n\t{3}", order.Time, user, order.Article, order.Comment));
                    }
                }
                Console.WriteLine();
            }
        }

        private void DoOrder()
        {
            Console.WriteLine("Bitte Artikel ID eingeben.");
            var artikelString = Console.ReadLine();
            Console.WriteLine("Bitte Anzahl eingeben.");
            var anzahlString = Console.ReadLine();
            Console.WriteLine("Bitte Comment eingeben.");
            var commentlString = Console.ReadLine();
            int artID = 0;
            int amount = 1;
            if (int.TryParse(artikelString, out artID))
            {
                if (int.TryParse(anzahlString, out amount))
                {
                    amount = System.Math.Abs(amount);
                    if (fachkonzept.GetArticles().Select(x => x.ID == artID).Count() != 0)
                        fachkonzept.CreateOrder(artID, amount, commentlString);
                    else
                        Console.WriteLine("Fehler: Artikel nicht gefunden.");
                }
                else
                {
                    Console.WriteLine("Fehler: Bitte Anzahl > 0 angeben.");
                }
            }
            else
            {
                Console.WriteLine("Fehler: Bitte nur gültige IDs angeben.");
            }

        }

        private void Checkout()
        {
            Console.WriteLine("Bitte Nr. der Bestellung eingeben.");
            var artikelString = Console.ReadLine();
            int artID = 0;
            if (int.TryParse(artikelString, out artID))
            {
                if (this.orders.Count() != 0 && artID > 1 && artID < this.orders.Count())
                {
                    fachkonzept.PurchaseItem(this.orders[artID - 1].ArticleId);
                    this.orders.RemoveAt(artID - 1);
                }
                else
                    Console.WriteLine("Fehler: Bestellung nicht gefunden.");
            }
            else
            {
                Console.WriteLine("Fehler: Bitte nur gültige Nr. angeben.");
            }
        }
    }
}
