﻿namespace DigiNote.Models.NSTui
{
    using System;
    using DigiNote.Interfaces;

    public class Login
    {
        private IFachkonzept fachkonzept;

        public Login(IFachkonzept fachkonzept)
        {
            this.fachkonzept = fachkonzept;
        }

        public void Show()
        {
            bool exit = false;
            do
            {
                Console.Clear();
                Console.WriteLine("Welcome to DigiNote!");
                Console.WriteLine("Anmelden       (1)");
                Console.WriteLine("Beenden      (ESC)");
                ConsoleKeyInfo x = Console.ReadKey();
                if (x.Key == ConsoleKey.Escape)
                    exit = true;
                else if (x.Key == ConsoleKey.D1)
                {
                    exit = DoLogin();
                    if (!exit)
                    {
                        Console.WriteLine("\nAnmeldung falsch!");
                        Console.ReadKey();
                    }
                }
            } while (!exit);
        }

        private bool DoLogin()
        {
            Console.Write("\nBenutzername: ");
            var user = Console.ReadLine();
            Console.Write("\nPasswort: ");
            string pass = EnterPassword();
            fachkonzept.ValidateUser(user, pass);
            if (fachkonzept.IsLoginSuccessfull)
                return true;
            else 
                return false;
        }

        internal static string EnterPassword()
        {
            string pass = "";
            ConsoleKeyInfo key;
            do
            {
                key = Console.ReadKey(true);
                if (key.Key != ConsoleKey.Backspace && key.Key != ConsoleKey.Enter)
                {
                    pass += key.KeyChar;
                    Console.Write("*");
                }
                else
                {
                    if (key.Key == ConsoleKey.Backspace && pass.Length > 0)
                    {
                        pass = pass.Substring(0, (pass.Length - 1));
                        Console.Write("\b \b");
                    }
                }
            } while (key.Key != ConsoleKey.Enter);

            return pass;
        }
    }
}