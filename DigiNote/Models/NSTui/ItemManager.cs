﻿namespace DigiNote.Models.NSTui
{
    using System;
    using System.Linq;
    using DigiNote.Interfaces;
    using System.Collections.Generic;
    using DigiNote.Interfaces.Business;

    public class ItemManager
    {
        private IFachkonzept fachkonzept;

        public ItemManager(IFachkonzept fachkonzept)
        {
            this.fachkonzept = fachkonzept;
        }

        private void PrintMenu()
        {
            Console.WriteLine("####  Artikelverwaltung 1    ####");
            Console.WriteLine("Artikelliste anzeigen         (1)"); // Artikel mit Gruppenzuordnungen
            Console.WriteLine("Gruppenliste anzeigen         (2)"); // Gruppen mit Artikelanzahl
            Console.WriteLine("---------------------------------");
            Console.WriteLine("Gruppe anlegen                (3)"); // Produkt Gruppe anlegen
            Console.WriteLine("Gruppe löschen                (4)"); // Produkt Gruppe löschen
            Console.WriteLine("---------------------------------");
            Console.WriteLine("Neuen Artikel anlegen         (5)"); // Artikel anlegen
            Console.WriteLine("Artikel löschen               (6)"); // Artikel löschen
            Console.WriteLine("---------------------------------");
            Console.WriteLine("Weiter                        (7)");
            Console.WriteLine("---------------------------------");
            Console.WriteLine("Zurück                        (x)");
        }

        private void PrintSubMenu()
        {
            Console.WriteLine("####   Artikelverwaltung 2   ####");
            Console.WriteLine("Artikel aller Gruppen anzeigen(1)");
            Console.WriteLine("Artikel von Gruppe anzeigen   (2)"); // Artikel Namen bearbeiten
            Console.WriteLine("Gruppen von Artikel anzeigen  (3)"); // Artikel Preis bearbeiten
            Console.WriteLine("Artikel in Gruppe einfügen    (4)");
            Console.WriteLine("Artikel aus Gruppe entfernen  (5)");
            Console.WriteLine("---------------------------------");
            Console.WriteLine("Zurück                        (x)");
        }

        public void Show()
        {
            bool exit = false;
            do
            {
                Console.Clear();
                PrintMenu();
                var key = Console.ReadKey();
                if (key.Key == ConsoleKey.D1)//Artikelliste anzeigen  
                {
                    Console.Clear();
                    ShowArticles();
                    Console.ReadKey();
                }
                if (key.Key == ConsoleKey.D2)//Gruppenliste anzeigen 
                {
                    Console.Clear();
                    ShowGroups();
                    Console.ReadKey();
                }
                if (key.Key == ConsoleKey.D3)// Produkt Gruppe anlegen   
                {
                    CreateGroup();
                }
                if (key.Key == ConsoleKey.D4)// Produkt Gruppe löschen 
                {
                    DeleteGroup();
                }
                if (key.Key == ConsoleKey.D5)// Artikel anlegen  
                {
                    CreateArticle();
                }
                if (key.Key == ConsoleKey.D6)// Artikel löschen  
                {
                    DeleteArticle();
                }
                if (key.Key == ConsoleKey.D7)//weiter  
                {
                    GroupMenu();
                }
                if (key.Key == ConsoleKey.X)//zurück 
                {
                    exit = true;
                }
            } while (!exit);
        }
        #region Menu 1
        private void ShowArticles()
        {
            var articles = fachkonzept.GetArticles();
            int nameLength = articles.Count() > 0 ? articles.Select(x => x.Name.Length).Max() : 0;

            nameLength = (nameLength > 6 ? nameLength : 6);

            string TEMPLATE = "|{0,4}|{1,-" + nameLength + "}|{2,15}|";
            string header = String.Format(TEMPLATE, " ID ", " Name ", " Preis in Euro ");
            Console.WriteLine(header);
            Console.WriteLine(String.Format("|{0}+{1}+{2}|", "----", new String('-', nameLength), new String('-', 15)));
            foreach (var article in articles)
            {
                Console.WriteLine(String.Format(TEMPLATE, article.ID, article.Name, article.Preis));
            }
        }

        private void ShowGroups()
        {
            var groups = fachkonzept.GetGroups();
            int nameLength = groups.Count() > 0 ? groups.Select(x => x.Name.Length).Max() : 0;

            nameLength = (nameLength > 6 ? nameLength : 6);

            string TEMPLATE = "|{0,4}|{1,-" + nameLength + "}|";
            string header = String.Format(TEMPLATE, " ID ", " Name ");
            Console.WriteLine(header);
            Console.WriteLine(String.Format("|{0}+{1}|", "----", new String('-', nameLength)));
            foreach (var group in groups)
            {
                Console.WriteLine(String.Format(TEMPLATE, group.ID, group.Name));
            }
        }

        private void CreateGroup()
        {
            Console.WriteLine("Bitte Gruppennamen angeben:");
            string name = Console.ReadLine();
            var valid = fachkonzept.ValidateNewGroup(name);
            if (valid == NewItemValidationStatus.Ok)
                fachkonzept.CreateGroup(name);
            else if (valid == NewItemValidationStatus.IsEmpty)
                Console.WriteLine("Fehler: Sie haben keinen Namen angegeben.");
            else if (valid == NewItemValidationStatus.AllreadyExisting)
                Console.WriteLine("Fehler: Der Gruppenname existiert bereits.");
            Console.ReadKey();
        }

        private void DeleteGroup()
        {
            Console.WriteLine("Löschen, bitte Gruppen ID angeben:");
            string idString = Console.ReadLine();
            int id = 0;
            if (int.TryParse(idString, out id))
            {

                if (fachkonzept.CanDeleteGroup(id))
                    fachkonzept.DeleteGroup(id);
                else
                    Console.WriteLine("Fehler: Gruppe kann nicht gelöscht werden. Gruppe ist nicht leer, oder nicht vorhanden.");
            }
            else
            {
                Console.WriteLine("Fehler: Sie haben einen ungültige ID angegeben.");
            }
            Console.ReadKey();
        }

        private void CreateArticle()
        {
            Console.WriteLine("Bitte Artikelnamen angeben:");
            string name = Console.ReadLine();
            Console.WriteLine("Bitte Preis angeben:");
            string preisString = Console.ReadLine();
            double preis = 0.0;
            if (double.TryParse(preisString, out preis))
            {
                var valid = fachkonzept.ValidateNewArticle(name);
                if (valid == NewItemValidationStatus.Ok)
                    fachkonzept.CreateArticle(name, preis);
                else if (valid == NewItemValidationStatus.IsEmpty)
                    Console.WriteLine("Fehler: Sie haben keinen Namen angegeben.");
                else if (valid == NewItemValidationStatus.AllreadyExisting)
                    Console.WriteLine("Fehler: Der Artikelname existiert bereits.");
            }
            else
            {
                Console.WriteLine("Fehler: Sie haben einen ungültigen Preis angegeben.");
            }
            Console.ReadKey();
        }

        private void DeleteArticle()
        {
            Console.WriteLine("Löschen, bitte Artikel ID angeben:");
            string idString = Console.ReadLine();
            int id = 0;
            if (int.TryParse(idString, out id))
            {

                if (fachkonzept.CanDeleteArticle(id))
                    fachkonzept.DeleteArticle(id);
                else
                    Console.WriteLine("Fehler: Artikel kann nicht gelöscht werden. Artikel ist nicht vorhanden, oder wird bestellt.");
            }
            else
            {
                Console.WriteLine("Fehler: Sie haben einen ungültige ID angegeben.");
            }
            Console.ReadKey();
        }
        #endregion //Menu 1

        private void GroupMenu() //TODO
        {
            bool exit = false;
            do
            {
                Console.Clear();
                PrintSubMenu();
                var key = Console.ReadKey();
                if (key.Key == ConsoleKey.D1)// Artikel Namen bearbeiten 
                {
                    ShowAllGroupContent();
                }
                if (key.Key == ConsoleKey.D2)//Artikel Preis bearbeiten 
                {
                    ShowArticlesFromGroup();
                }
                if (key.Key == ConsoleKey.D3)//Artikel:Gruppe1 (ID), Gruppe2(ID)   
                {
                    ShowGroupsFromArticle();
                }
                if (key.Key == ConsoleKey.D4)// Artikel aus Gruppe entfernen 
                {
                    AddArticleToGroup();
                }
                if (key.Key == ConsoleKey.D5)// Artikel aus Gruppe entfernen 
                {
                    DeleteArticleFromGroup();
                }
                if (key.Key == ConsoleKey.X)//zurück 
                {
                    exit = true;
                }
            } while (!exit);
        }

        private void ShowAllGroupContent()
        {
            var groups = fachkonzept.GetGroups();
            var articles = fachkonzept.GetArticles();

            int nameLength = groups.Count() > 0 ? groups.Select(x => x.Name.Length).Max() : 0;

            nameLength = (nameLength > 6 ? nameLength : 6);

            string TEMPLATE = "|{0,4}|{1,-" + nameLength + "}|";
            string header = String.Format(TEMPLATE, " ID ", " Name ");
            Console.WriteLine("\n" + header);
            Console.WriteLine(String.Format("|{0}+{1}|", "----", new String('-', nameLength)));
            foreach (var group in groups)
            {

                Console.WriteLine(String.Format("\n" + TEMPLATE, group.ID, group.Name));
                Console.WriteLine(String.Format("|{0}+{1}|", "----", new String('-', nameLength)));
                PrintArticlesFromGroup(articles);
            }

            Console.ReadKey();
        }

        private void PrintArticlesFromGroup(IEnumerable<IArticle> articles)
        {

            int nameLength = articles.Count() > 0 ? articles.Select(x => x.Name.Length).Max() : 0;

            nameLength = (nameLength > 6 ? nameLength : 6);

            string TEMPLATE = "|{0,4}|{1,-" + nameLength + "}|{2,15}|";
            string header = String.Format(TEMPLATE, " ID ", " Name ", " Preis in Euro ");
            Console.WriteLine(header);
            Console.WriteLine(String.Format("|{0}+{1}+{2}|", "----", new String('-', nameLength), new String('-', 15)));
            foreach (var article in articles)
            {
                Console.WriteLine(String.Format(TEMPLATE, article.ID, article.Name, article.Preis));
            }
        }

        private void ShowArticlesFromGroup()
        {
            Console.WriteLine("\nBitte Gruppen ID eingeben:");
            var idString = Console.ReadLine();
            int id = 0;
            var group = fachkonzept.GetGroups();
            var articles = fachkonzept.GetArticles();
            if (int.TryParse(idString, out id))
            {
                if (group.Where(x => x.ID == id).Count() != 0)
                {
                    PrintArticlesFromGroup(fachkonzept.GetArticlesFromGroup(id));
                }
                else
                {
                    Console.WriteLine("Fehler: Gruppe nicht gefunden.");
                }
            }
            else
            {
                Console.WriteLine("Fehler: Bitte geben Sie eine gültige Gruppen ID an.");
            }
            Console.ReadKey();
        }

        private void ShowGroupsFromArticle()
        {
            Console.WriteLine("\nBitte Artikel ID eingeben:");
            var idString = Console.ReadLine();
            int id = 0;
            var articles = fachkonzept.GetArticles();
            if (int.TryParse(idString, out id))
            {
                if (articles.Where(x => x.ID == id).Count() != 0)
                {
                    var groups = fachkonzept.GetGroupsFromArticle(id);
                    int nameLength = groups.Count() > 0 ? groups.Select(x => x.Name.Length).Max() : 0;

                    nameLength = (nameLength > 6 ? nameLength : 6);

                    string TEMPLATE = "|{0,4}|{1,-" + nameLength + "}|";
                    string header = String.Format(TEMPLATE, " ID ", " Name ");
                    Console.WriteLine(header);
                    Console.WriteLine(String.Format("|{0}+{1}|", "----", new String('-', nameLength)));
                    foreach (var group in groups)
                    {
                        Console.WriteLine(String.Format(TEMPLATE, group.ID, group.Name));
                    }

                }
                else
                {
                    Console.WriteLine("Fehler: Gruppe nicht gefunden.");
                }
            }
            else
            {
                Console.WriteLine("Fehler: Bitte geben Sie eine gültige Gruppen ID an.");
            }
            Console.ReadKey();
        }

        private void AddArticleToGroup()
        {
            Console.WriteLine("Bitte Artikel ID eingeben:");
            var articleString = Console.ReadLine();
            Console.WriteLine("Bitte Gruppen ID eingeben:");
            var groupString = Console.ReadLine();
            var articles = fachkonzept.GetArticles();
            int articleID = 0;
            int groupID = 0;
            if (int.TryParse(articleString, out articleID) && int.TryParse(groupString, out groupID))
            {
                try
                {
                    fachkonzept.AddArticleToGroup(groupID, articleID);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Fehler: " + ex.Message);
                }
            }
            else
            {
                Console.WriteLine("Fehler: Bitte nur gültige IDs eingeben!");
            }
        }

        private void DeleteArticleFromGroup()
        {
            Console.WriteLine("Bitte Artikel ID eingeben:");
            var articleString = Console.ReadLine();
            Console.WriteLine("Bitte Gruppen ID eingeben:");
            var groupString = Console.ReadLine();
            var articles = fachkonzept.GetArticles();
            int articleID = 0;
            int groupID = 0;
            if (int.TryParse(articleString, out articleID) && int.TryParse(groupString, out groupID))
            {
                try
                {
                    fachkonzept.RemouveArticleFromGroup(groupID, articleID);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Fehler: " + ex.Message);
                }
            }
            else
            {
                Console.WriteLine("Fehler: Bitte nur gültige IDs eingeben!");
            }
        }
    }
}
