﻿namespace DigiNote.Models.NSTui
{
    using System;
    using System.Linq;
    using DigiNote.Interfaces;
    using System.Collections.Generic;
    using DigiNote.Interfaces.Business;

    public class Home
    {
        IFachkonzept fachkonzept;
        private List<OrderTVM> orders;
        public Home(IFachkonzept fachkonzept)
        {
            this.fachkonzept = fachkonzept;
            orders = new List<OrderTVM>();
        }

        private void PrintMenu()
        {
            Console.WriteLine("####   Hauptmenu:   ####");
            Console.WriteLine("Profilverwaltung     (1)");
            Console.WriteLine("Artikelverwaltung    (2)");
            Console.WriteLine("Einkaufszettel       (3)");
            Console.WriteLine("Abmelden             (4)");
            Console.WriteLine("Beenden            (ESC)");
        }

        

        public void Show()
        {
            bool exit = false;
            do
            {
                Console.Clear();
                OrderManager.PrintOrders(this.orders,this.fachkonzept);
                OrderManager.PrintComments(this.fachkonzept);
                PrintMenu();
                var key = Console.ReadKey();
                if (key.Key == ConsoleKey.D1)//Profilverwaltung 
                {
                    var pm = new ProfileManager(fachkonzept);
                    pm.Show();
                }
                if (key.Key == ConsoleKey.D2)//Artikelverwaltung 
                {
                    var am = new ItemManager(fachkonzept);
                    am.Show();
                }
                if (key.Key == ConsoleKey.D3)//Einkaufszettel 
                {
                    var om = new OrderManager(fachkonzept,this.orders);
                    om.Show();
                }
                if (key.Key == ConsoleKey.D4)//Abmelden 
                {
                    fachkonzept.PerformLogOut();
                    exit = true;
                }
                if (key.Key == ConsoleKey.Escape)//Beenden 
                {
                    exit = true;
                }
                //fachkonzept.PerformLogOut();
            } while (!exit);
        }
    }
}
