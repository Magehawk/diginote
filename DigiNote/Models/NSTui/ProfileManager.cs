﻿namespace DigiNote.Models.NSTui
{
    using System;
    using System.Linq;
    using DigiNote.Interfaces;

    public class ProfileManager
    {
        private IFachkonzept fachkonzept;

        public ProfileManager(IFachkonzept fachkonzept)
        {
            this.fachkonzept = fachkonzept;
            Console.Clear();
        }

        private void PrintProfiles()
        {
            var profiles = fachkonzept.GetProfiles();
            int userLength = profiles.Count() > 0 ? profiles.Select(x => x.UserName.Length).Max() : 0;
            int firstLength = profiles.Count() > 0 ? profiles.Select(x => x.FirstName.Length).Max() : 0;
            int lastLength = profiles.Count() > 0 ? profiles.Select(x => x.LastName.Length).Max() : 0;
            userLength = (userLength > 14 ? userLength : 14);
            firstLength = (firstLength > 9 ? firstLength : 9);
            lastLength = (lastLength > 10 ? lastLength : 10);
            string TEMPLATE = "|{0,4}|{1,-" + userLength + "}|{2,-" + firstLength + "}|{3,-" + lastLength + "}|";
            string header = String.Format(TEMPLATE, " ID ", " Benutzername ", " Vorname ", " Nachname ");
            Console.WriteLine(header);
            Console.WriteLine(String.Format("|{0}+{1}+{2}+{3}|", "----", new String('-', userLength), new String('-', firstLength), new String('-', lastLength)));
            foreach (var profile in profiles)
            {
                Console.WriteLine(String.Format(TEMPLATE, profile.ID, profile.UserName, profile.FirstName, profile.LastName));
            }

        }

        private void PrintMenu()
        {
            PrintProfiles();
            Console.WriteLine("\n#### Profilverwaltung ####");
            Console.WriteLine("Neues Profil anlegen   (1)");
            Console.WriteLine("Profil bearbeiten      (2)");
            Console.WriteLine("Profil löschen         (3)");
            Console.WriteLine("--------------------------");
            Console.WriteLine("Zurück                 (x)");
        }

        public void Show()
        {
            bool exit = false;
            do
            {
                Console.Clear();
                PrintMenu();
                var key = Console.ReadKey();
                if (key.Key == ConsoleKey.D1)//Neues Profil anlegen  
                {
                    CreateProfile();
                }
                if (key.Key == ConsoleKey.D2)//Profil bearbeiten 
                {
                    EditProfile();
                }
                if (key.Key == ConsoleKey.D3)//Profil löschen  
                {
                    DeleteProfile();
                }
                if (key.Key == ConsoleKey.X)//zurück 
                {
                    exit = true;
                }
            } while (!exit);
        }

        private void CreateProfile()
        {
            Console.WriteLine();
            Console.Write("\nBenutzername: ");
            var user = Console.ReadLine();
            Console.Write("\nVorname: ");
            var firstName = Console.ReadLine();
            Console.Write("\nNachname: ");
            var lastName = Console.ReadLine();
            Console.Write("\nNeues Passwort: ");
            var pass = Login.EnterPassword();
            Console.Write("\nPasswort bestätigen: ");
            var confPass = Login.EnterPassword();
            var valid = fachkonzept.ValidateNewPofile(user, pass, confPass);
            if (valid == NewProfileValidationStatus.Ok)
            {
                var newProfile = fachkonzept.CreateProfile();
                fachkonzept.EditProfile(newProfile.ID, user, firstName, lastName, pass, null);
                Console.WriteLine("\nProfil erfolgreich angelegt.");
            }
            else if (valid == NewProfileValidationStatus.PasswordFailed)
            {
                Console.WriteLine("\nFehler: Passwörter stimmen nicht überein.");
            }
            else if (valid == NewProfileValidationStatus.AllreadyExisting)
            {
                Console.WriteLine("\nFehler: Benutzername bereits vergeben.");
            }
            Console.ReadKey();
        }

        private void EditProfile()
        {
            Console.WriteLine("Bearbeiten von Profil " + fachkonzept.CurentUser.UserName + ":");
            Console.Write("\nBenutzername: ");
            var user = Console.ReadLine();
            Console.Write("\nVorname: ");
            var firstName = Console.ReadLine();
            Console.Write("\nNachname: ");
            var lastName = Console.ReadLine();
            Console.Write("\nNeues Passwort: ");
            var pass = Login.EnterPassword();
            Console.Write("\nPasswort bestätigen: ");
            var confPass = Login.EnterPassword();
            var valid = fachkonzept.ValidateNewPofile(user, pass, confPass);
            var ex = (valid == NewProfileValidationStatus.AllreadyExisting);
            if (valid == NewProfileValidationStatus.Ok || (ex && fachkonzept.CurentUser.UserName == user))
            {
                fachkonzept.EditProfile(fachkonzept.CurentUser.ID, user, firstName, lastName, pass, fachkonzept.CurentUser.Image);
                Console.WriteLine("\nProfil erfolgreich bearbeitet.");
            }
            else if (valid == NewProfileValidationStatus.PasswordFailed)
            {
                Console.WriteLine("\nFehler: Passwörter stimmen nicht überein.");
            }
            else if (ex)
            {
                Console.WriteLine("\nFehler: Benutzername bereits vergeben.");
            }
            Console.ReadKey();
        }

        private void DeleteProfile()
        {
            Console.WriteLine("Bitte Profil ID eingeben:");
            var input = Console.ReadLine();
            int id;
            if (int.TryParse(input, out id))
            {
                if (fachkonzept.CanDeleteProfile(id))
                {
                    fachkonzept.DeleteProfile(id);
                    Console.WriteLine("Profil erfolgreich gelöscht");
                }
                else
                {
                    if (fachkonzept.CurentUser.ID == id)
                        Console.WriteLine("Sie können ihr eigenes Profil nicht löschen!");
                    else
                        Console.WriteLine("Es sind noch Bestellungen offen.");
                }
            }
            else
            {
                Console.WriteLine("Bitte geben Sie nur gültige IDs ein.");
            }
            Console.ReadKey();
        }

    }
}
