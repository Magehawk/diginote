﻿namespace DigiNote.Models.NSTui
{
    public class OrderTVM
    {
        public string Name { get; set; }

        public int ArticleId { get; set; }

        public int Amount { get; set; }

        public double Price { get; set; }
    }
}
