﻿namespace DigiNote.Models
{
    using System;

    internal static class LazyEncryption
    {
        internal static string Crypt(string baseString)
        {
            byte[] stringBytes = new byte[baseString.Length * sizeof(char)];
            Buffer.BlockCopy(baseString.ToCharArray(), 0, stringBytes, 0, stringBytes.Length);
            return Convert.ToBase64String(stringBytes);
        }

        internal static string Decrypt(string lazyCryptedString)
        {
            byte[] stringBytes = Convert.FromBase64String(lazyCryptedString);
            char[] baseArray= new char[stringBytes.Length/sizeof(char)];
            Buffer.BlockCopy(stringBytes, 0, baseArray, 0, stringBytes.Length);
            return new string(baseArray);
        }

    }
}
