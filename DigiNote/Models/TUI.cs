﻿namespace DigiNote.Models
{
    using System;
    using System.Runtime.InteropServices;
    using DigiNote.Interfaces;
    using DigiNote.Models.NSTui;

    public class TUI
    {
        #region DllImport
        [DllImport("Kernel32")]
        public static extern void AllocConsole();

        [DllImport("Kernel32")]
        public static extern bool AttachConsole(int id);

        [DllImport("Kernel32")]
        public static extern void FreeConsole();
        #endregion //DllImport

        IFachkonzept fachkonzept;

        public TUI(IFachkonzept fachkonzept)
        {
            this.fachkonzept = fachkonzept;
            AllocConsole();
            this.Run();
            FreeConsole();
        }

        private void Run()
        {
            bool exit = false;
            do
            {
                Console.Clear();
                var login = new Login(fachkonzept);
                login.Show();
                if (fachkonzept.IsLoginSuccessfull)
                {
                    var Home = new Home(fachkonzept);
                    fachkonzept.PerformLogIn();
                    Home.Show();
                    exit = !fachkonzept.IsUserChanged;
                }
                else
                    exit = true;

            } while (!exit);
        }
    }
}
