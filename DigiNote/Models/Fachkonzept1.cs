﻿namespace DigiNote.Models
{
    using DigiNote.Interfaces;
    using DigiNote.Interfaces.Business;
    using DigiNote.Models.Business;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Media.Imaging;
    using System;

    public class Fachkonzept1 : IFachkonzept
    {
        private IDatenhaltung datenhaltung;
        private List<IProfile> profiles;
        private IProfile activeUser;
        private List<IArticle> articles;
        private List<IGroup> groups;
        private List<IOrder> orders;

        public Fachkonzept1(IDatenhaltung datenhaltung)
        {
            this.datenhaltung = datenhaltung;
            this.profiles = datenhaltung.ReadAllUsers();
            this.articles = datenhaltung.ReadAllArticles();
            this.groups = datenhaltung.ReadAllGroups();
            this.orders = datenhaltung.ReadAllOrders();
            //activeUser = new Profile();
            IsUserChanged = false;
        }

        #region SessionManagement
        public IProfile CurentUser
        {
            get { return this.activeUser; }
            private set { this.activeUser = value; }
        }
        public bool IsLoginSuccessfull { get; private set; }
        public bool IsUserChanged { get; private set; }
        public void ValidateUser(string userName, string password)
        {
            if (userName == "root" && password == "diginote")
            {
                activeUser = new Profile();
                IsLoginSuccessfull = true;
                IsUserChanged = false;
            }
            else
            {
                var validUsers = datenhaltung.ReadAllUsers().Where(x => x.UserName == userName && x.Password == password);
                if (validUsers.Count() != 0)
                {
                    activeUser = validUsers.First();
                    IsLoginSuccessfull = true;
                    IsUserChanged = false;
                }
                else
                {
                    IsLoginSuccessfull = false;
                    IsUserChanged = false;
                }
            }
        }

        public void PerformLogIn()
        {
            IsLoginSuccessfull = false;
            IsUserChanged = false;
        }

        public void PerformLogOut()
        {
            IsLoginSuccessfull = false;
            IsUserChanged = true;
        }

        #endregion //SessionManagement

        #region ProfileManagement
        public IEnumerable<IProfile> GetProfiles()
        {
            return profiles;
        }
        public NewProfileValidationStatus ValidateNewPofile(string userName, string password, string confirmPassword)
        {
            var name = profiles.Where(x => x.UserName == userName);
            if (name.Count() != 0)
                return NewProfileValidationStatus.AllreadyExisting;
            if (password != confirmPassword)
                return NewProfileValidationStatus.PasswordFailed;
            return NewProfileValidationStatus.Ok;
        }
        public IProfile CreateProfile()
        {
            var newProfile = new Profile();
            newProfile.ID = GenerateID(profiles, 0);
            profiles.Add(newProfile);
            return newProfile;
        }
        public void EditProfile(int id, string userName, string firstName, string lastName, string password, BitmapSource image)
        {
            Profile profile=(Profile)profiles.Where(x => x.ID == id).First();
            profile.UserName = userName;
            profile.FirstName = firstName;
            profile.LastName = lastName;
            profile.Password = password;
            profile.Image = image;
            datenhaltung.SaveProfile(profile);
        }
        public bool CanDeleteProfile(int id)
        {
            var profile = profiles.Where(x => x.ID == id).First();
            if (activeUser.UserName == "root")
                return false;
            else
            if (activeUser == profile)
                return false;
            else
            {
                var orders = GetOrders().Where(x => x.ProfileId == id);
                if (orders.Count() != 0)
                    return false;
                else
                    return true;
            }
        }
        public void DeleteProfile(int id)
        {
            if (CanDeleteProfile(id))
                datenhaltung.RemoveProfile(id);
        }
        #endregion //ProfileManagement

        #region GroupManagement
        public IEnumerable<IGroup> GetGroups()
        {
            return this.groups;
        }
        public NewItemValidationStatus ValidateNewGroup(string name)
        {
            if (!string.IsNullOrWhiteSpace(name))
            {
                var names = groups.Where(x => x.Name == name);
                if (names.Count() != 0)
                    return NewItemValidationStatus.AllreadyExisting;
                else
                    return NewItemValidationStatus.Ok;
            }
            else
                return NewItemValidationStatus.IsEmpty;
        }
        public IGroup CreateGroup(string name)
        {
            if (ValidateNewGroup(name) == NewItemValidationStatus.Ok)
            {
                int id = GenerateID(groups, 0);
                var newGroup = new Group(id, name);
                groups.Add(newGroup);
                datenhaltung.SaveGroup(newGroup);
                return newGroup;
            }
            else
                throw new System.ArgumentException();
        }
        public void EditGroup(int id, string name)
        {
            var group = groups.Where(x => x.ID == id).First();
            group.Name = name;
            datenhaltung.SaveGroup(group);
        }
        public bool CanDeleteGroup(int id)
        {
            var group = groups.Where(x => x.ID == id).First();

            if (group.Articles.Count != 0)
                return false;
            else
                return true;
        }
        public void DeleteGroup(int id)
        {
            if (CanDeleteGroup(id)) 
            {
                groups.Remove(groups.Where(x => x.ID == id).First());
                datenhaltung.RemoveGroup(id);
            }
        }
        
        /// <summary>
        /// Fügt einer Gruppe einen Arikel hinzu.
        /// Wirft eine System.ArgumentException(), wenn die groupId oder articleId nicht gefunden werden kann. 
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="articleId"></param>
        public void AddArticleToGroup(int groupId, int articleId)
        {
            var group = groups.Where(x => x.ID == groupId);
            if (group.Count() == 0)
                throw new System.ArgumentException("Gruppe "+groupId+" ist nicht vorhanden.");
            var article = articles.Where(x => x.ID == articleId);
            if (article.Count() == 0)
                throw new System.ArgumentException("Artikel " + articleId + " ist nicht vorhanden.");
            
            if (!group.First().Articles.Contains(articleId))
            {
                group.First().Articles.Add(articleId);
                datenhaltung.SaveGroup(group.First());
            }
        }
        
        /// <summary>
        /// Entfernt einen Arikel aus einer Gruppe.
        /// Wirft eine System.ArgumentException(), wenn die groupId oder articleId nicht gefunden werden kann. 
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="articleId"></param>
        public void RemouveArticleFromGroup(int groupId, int articleId)
        {
            var group = groups.Where(x => x.ID == groupId);
            if (group.Count() == 0)
                throw new System.ArgumentException("Gruppe " + groupId + " ist nicht vorhanden.");
            var article = articles.Where(x => x.ID == articleId);
            if (article.Count() == 0)
                throw new System.ArgumentException("Artikel " + articleId + " ist nicht vorhanden.");

            if (group.First().Articles.Contains(articleId))
            {
                group.First().Articles.Remove(articleId);
                datenhaltung.SaveGroup(group.First());
            }
        }

        public IEnumerable<IArticle> GetArticlesFromGroup(int groupId)
        {
            var articleIds = groups.Where(x => x.ID == groupId).First().Articles;

            var list = new List<IArticle>();

            foreach (int id in articleIds) 
            {
                list.Add(articles.Where(x => x.ID == id).First());
            }

            return list;
        }
        #endregion //GroupManagement

        #region ArticleManagement
        public IEnumerable<IArticle> GetArticles()
        {
            return this.articles;
        }
        public NewItemValidationStatus ValidateNewArticle(string name)
        {
            var names = articles.Where(x => x.Name == name);
            if (names.Count() != 0)
                return NewItemValidationStatus.AllreadyExisting;
            else
                return NewItemValidationStatus.Ok;
        }
        public IArticle CreateArticle(string name, double price)
        {
            int id = GenerateID(articles, 0);
            var newArticle = new Article(id, name);
            newArticle.Preis = price;
            articles.Add(newArticle);
            datenhaltung.SaveArticle(newArticle);
            return newArticle;
        }
        public void EditArticle(int id, string name, double price)
        {
            Article article = articles.Where(x => x.ID == id).First() as Article;
            article.Name = name;
            article.Preis = price;
            datenhaltung.SaveArticle(article);
        }
        public bool CanDeleteArticle(int id)
        {
            var article = articles.Where(x => x.ID == id);
            if (article.Count() != 0)
            {
                var groupsWithArticle = groups.Where(x => x.Articles.Contains(id));
                if (groupsWithArticle.Count() != 0)
                    return false;
                var ordersWithArticle = orders.Where(x => x.ArticleId == id);
                if (ordersWithArticle.Count() != 0)
                    return false;
                return true;
            }
            else 
            {
                return false;
            }
        }
        public void DeleteArticle(int id)
        {
            if (CanDeleteArticle(id))
            {
                articles.Remove(articles.Where(x => x.ID == id).First());
                datenhaltung.RemoveArticle(id);
            }
        }
        public IEnumerable<IGroup> GetGroupsFromArticle(int articleId)
        {
            return groups.Where(x => x.Articles.Contains(articleId));
        }
        #endregion //ArticleManagement

        #region OrderManagement
        public IEnumerable<IOrder> GetOrders()
        {
            this.orders = datenhaltung.ReadAllOrders();
            return orders;
        }
        public IOrder CreateOrder(int articleId, int ammount, string comment)
        {
            var order = new Order(activeUser.ID,articleId,Math.Abs(ammount));
            order.Comment=comment;
            order.CommentTimeStamp=DateTime.Now;
            datenhaltung.SaveOrder(order);
            return order;
        }
        public void PurchaseItem(int articleId)
        {
            var filteredOrders = orders.Where(x => x.ArticleId == articleId);
            foreach(var order in filteredOrders)
            {
                if(order.ArticleId==articleId)
                    datenhaltung.RemoveOrder(order);
            }
            
        }
        #endregion //OrderManagement

        #region private Methods
        private int GenerateID(IEnumerable<IHaveId> list, int id)
        {
            int newId = id;
            if (list.Where(x => x.ID == id).Count() != 0)
                newId = GenerateID(list, id + 1);
            return newId;
        }
        #endregion //private Methods
    }
}
